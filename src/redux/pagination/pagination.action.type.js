const PaginationActionType = {
    INCREMENT_PAGE_NO: "INCREMENT_PAGE_NO",
    DECREMENT_PAGE_NO: "DECREMENT_PAGE_NO"
}

export default PaginationActionType