import LocationActionType from './location.action.type'

const INITIAL_STATE = {
    locationList:[]
}


const paginationReducer =(state=INITIAL_STATE,action)=>{
    switch(action.type){
        case LocationActionType.STORE_LOCATIONS:{
            return{
                ...state,
                [action.payload]: state[action.payload] + 1
            }
        }
        default:{
            return state
        }
    }
}

export default paginationReducer