import React, { useEffect, useRef, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Form from 'react-bootstrap/Form'
// reactstrap components
import {
  // Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,


  Row,
  Col,
} from "reactstrap";
import { useHistory } from "react-router";
import KK_Toast from "components/Toast/KK_Toast";
import apis from "apis";
import { loginUserRequest } from "redux/login/login.action";
import { loginUserSuccess } from "redux/login/login.action";
import { loginUserFailure } from "redux/login/login.action"
import { connect } from "react-redux";
import { InputGroup } from "react-bootstrap";
import { validation } from "components/_helpers/validation";



const useStyles = makeStyles({
  root: {
    background: 'linear-gradient(45deg, #7c4dff 30%, #b388ff  90%)',
    border: 0,
    borderRadius: 10,
    boxShadow: '0 3px 5px 2px #b488ff83',
    color: 'white',
    height: 48,
    padding: '0 30px',
  },
});


const Login = ({ loginRequest, loginSuccess, loginFailure, loading }) => {
  let emailRef = useRef(null)

  useEffect(() => {
    emailRef.current.focus()
    console.log(emailRef)
  }, [])
  let history = useHistory()
  const classes = useStyles();
  const loginRef = useRef();
  const [toastConfig, setToastConfig] = useState({
    toastStatus: false,
    message: "",
    severity: null,
  })

  const [loginData, setLoginData] = useState({
    email: null,
    password: null
  })
  const login = async (e) => {
    loginRequest()
    setToastConfig({
      ...toastConfig,
      toastStatus: true,
      message: "Authenticating...",
      severity: "info"
    })

    let data = {
      user_name: loginData.email,
      password: loginData.password
    }
    try {
      let res = await apis.authentication.loginRequest(data);
      console.log(res)
      if (res.success) {
        loginSuccess(res.data)
        setToastConfig({
          ...toastConfig,
          toastStatus: true,
          message: res.message,
          severity: "success"
        })
        return setTimeout(() => {
          history.push('/admin/')
        }, 2000)
      } else {
        loginFailure(res.data)
        setToastConfig({
          ...toastConfig,
          toastStatus: true,
          message: res.message,
          severity: "error"
        })
      }
      console.log(res)
    } catch (err) {
      console.log(err)
      setToastConfig({
        ...toastConfig,
        toastStatus: true,
        message: err.message,
        severity: "error"
      })
    }
  }

  useEffect(() => {
    console.log(toastConfig)
  }, [toastConfig])

  useEffect(() => {
    console.log("Initial LOgin DAta", loginData)
  }, [loginData])

  const onChangeHandler = (e) => {
    if(e.target.name === "email"){
      setLoginData({
        ...loginData,
        email: e.target.value
      })
    }

      let res;
      if(e.target.name === "password"){
         res=validation.spacesCheck(e.target.value,"password")
      console.log(res)
      console.log(res&&res.validation===false)
      if(res&&res.validation===false){
        setToastConfig({
          ...toastConfig,
          toastStatus: true,
          message: "Only Alphabets are allowed",
          severity: "error"
        })
      }else{
        setLoginData({
          ...loginData,
          password: e.target.value
        })
      }
    }
  }

  useEffect(()=>{
    console.log(loginData)
  },[loginData])

const handleKeyPress =(e) =>{
  if(e.code==="Enter"){
    login()
  }
}

  return (
    <>
      <Col xl="5" lg="5" md="7" sm="10" xs="11">
        <KK_Toast setToastConfig={setToastConfig} toastConfig={toastConfig} />

        <Card className=" border-0"
          style={{
            boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 5px 20px 0 rgba(0, 0, 0, 0.19)",
            // boxShadow: '0 3px 5px 2px #75757983',

            borderRadius: "5%",
            backgroundColor: "#ede7f6",

          }}
        >
          <CardHeader className="bg-transparent  pb-5">
            <div className="text-center ">
              <h1>
                Log In
               </h1>
            </div>

          </CardHeader>
          <CardBody className="px-lg-5 py-lg-5">
            {/* <div className="text-center text-muted mb-4">
              <small>Or sign in with credentials</small>
            </div> */}
            <Form>
              <Form.Group controlId="formBasicEmail" className="mb-5">
                {/* <Form.Label>Email address</Form.Label> */}
                <InputGroup hasValidation>
                  <InputGroup.Prepend>
                    <InputGroup.Text><i class="fas fa-envelope"></i></InputGroup.Text>
                  </InputGroup.Prepend>
                <Form.Control ref={emailRef} type="email" onKeyPress={handleKeyPress} name="email" onChange={onChangeHandler} placeholder="Enter username" required />
                </InputGroup>
                {/* <Form.Control.Feedback type="invalid">
                  Please choose a username.
                </Form.Control.Feedback> */}
              </Form.Group>

              <Form.Group controlId="formBasicPassword" className="mb-5">
                {/* <Form.Label>Password</Form.Label> */}
                <InputGroup hasValidation>
                  <InputGroup.Prepend>
                    <InputGroup.Text><i class="fas fa-unlock-alt"></i></InputGroup.Text>
                  </InputGroup.Prepend>
                  <Form.Control type="password" name="password" value={loginData.password} onKeyPress={handleKeyPress} onChange={onChangeHandler} placeholder="Password" />
                </InputGroup>
              </Form.Group>

              <Form.Group className="text-center">
                <Button
                  onClick={login}
                  className={classes.root}
                // ref={loginRef}
                >Log In</Button>
              </Form.Group>
            </Form>
            {/* </div> */}
          </CardBody>
        </Card>
        {/* <Row className="mt-3">
          <Col xs="6">
            <a
              className="text-light"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <small>Forgot password?</small>
            </a>
          </Col>
          <Col className="text-right" xs="6">
            <a
              className="text-light"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <small>Create new account</small>
            </a>
          </Col>
        </Row> */}
      </Col>
    </>
  );
};

const mapStateToProps = state => {
  return {
    loading: state.login.loading
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loginRequest: () => { dispatch(loginUserRequest()) },
    loginSuccess: (data) => { dispatch(loginUserSuccess(data)) },
    loginFailure: (error) => { dispatch(loginUserFailure(error)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
