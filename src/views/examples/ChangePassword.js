import React, { useEffect, useRef, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Form from 'react-bootstrap/Form'
// reactstrap components
import {
  // Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Row,
  Col,
} from "reactstrap";
import { useHistory } from "react-router";
import KK_Toast from "components/Toast/KK_Toast";
import apis from "apis";
import { loginUserRequest } from "redux/login/login.action";
import { loginUserSuccess } from "redux/login/login.action";
import { loginUserFailure } from "redux/login/login.action"
import { connect } from "react-redux";
import { InputGroup } from "react-bootstrap";
import { validation } from "components/_helpers/validation";
import { logout } from "redux/login/login.action";



const useStyles = makeStyles({
  root: {
    background: 'linear-gradient(45deg, #7c4dff 30%, #b388ff  90%)',
    border: 0,
    borderRadius: 10,
    boxShadow: '0 3px 5px 2px #b488ff83',
    color: 'white',
    height: 48,
    padding: '0 30px',
  },
});


const ChangePassword = ({ loginRequest, loginSuccess, loginFailure, loading,user_id,logout }) => {
  let emailRef = useRef(null)
  console.log("user_id",user_id)
  useEffect(() => {
    emailRef.current.focus()
    console.log(emailRef)
  }, [])
  let history = useHistory()
  const classes = useStyles();
  const loginRef = useRef();
  const [toastConfig, setToastConfig] = useState({
    toastStatus: false,
    message: "",
    severity: null,
  })

  const [passwords, setPasswords] = useState({
    password: null,
    rePassword: null
  })
  const changePassword = async (e) => {
    if(passwords.password!==passwords.rePassword){
        return setToastConfig({
        ...toastConfig,
        toastStatus: true,
        message: "Password didnt matched",
        severity: "error"
      })
    }
    
    if(passwords.password.length<=7){
       return setToastConfig({
        ...toastConfig,
        toastStatus: true,
        message: "Password Should have atleast 8 characters",
        severity: "error"
      })
    }
 

    let data = {
      user_id:user_id,
      password: passwords.password,
    }
    try {
      let res = await apis.superAdmin.changePassword(data);
      console.log(res)
      if (res.resultCode===1) {
        setToastConfig({
          ...toastConfig,
          toastStatus: true,
          message: res.resultMsg,
          severity: "success"
        })
        return setTimeout(() => {
          logout();
        }, 2000)
      } else {
        setToastConfig({
          ...toastConfig,
          toastStatus: true,
          message: res.message,
          severity: "error"
        })
      }
      console.log(res)
    } catch (err) {
      console.log(err)
      setToastConfig({
        ...toastConfig,
        toastStatus: true,
        message: err.message,
        severity: "error"
      })
    }
  }

  useEffect(() => {
    console.log(toastConfig)
  }, [toastConfig])

  useEffect(() => {
    console.log("Initial LOgin DAta", passwords)
  }, [passwords])

  const onChangeHandler = (e) => {
    if(e.target.name === "password"){
      setPasswords({
        ...passwords,
        password: e.target.value
      })
    }else{
          setPasswords({
            ...passwords,
            rePassword: e.target.value
          })
    }
  }



const handleKeyPress =(e) =>{
  if(e.code==="Enter"){
    changePassword()
  }
}

  return (
    <>
    <div  >
      <div style={{height:"64px"}}></div>
      <Row style={{width:"100%", display:"flex", justifyContent:"center", marginTop:"64px"}}>
    <Col xl="5 " lg="5" md="7" sm="10" xs="11">
        <KK_Toast style={{position:"absolute", top:"0"}} setToastConfig={setToastConfig} toastConfig={toastConfig} />

        <Card className=" border-0"
          style={{
            boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 5px 20px 0 rgba(0, 0, 0, 0.19)",
            // boxShadow: '0 3px 5px 2px #75757983',

            borderRadius: "5%",
            backgroundColor: "#ede7f6",

          }}
        >
          <CardHeader className="bg-transparent ">
            <div className="text-center ">
              <h1>
                Change Password
               </h1>
            </div>

          </CardHeader>
          <CardBody className="px-lg-5 py-lg-5">
            {/* <div className="text-center text-muted mb-4">
              <small>Or sign in with credentials</small>
            </div> */}
            <Form>
              <Form.Group controlId="formBasicEmail" className="mb-5">
                <Form.Label>Email New Password</Form.Label>
                <InputGroup hasValidation>
                  <InputGroup.Prepend>
                    <InputGroup.Text><i class="fas fa-unlock-alt"></i></InputGroup.Text>
                  </InputGroup.Prepend>
                <Form.Control ref={emailRef} type="password" onKeyPress={handleKeyPress} value={passwords.password} name="password" onChange={onChangeHandler} placeholder="Enter Password" required />
                </InputGroup>
                {/* <Form.Control.Feedback type="invalid">
                  Please choose a username.
                </Form.Control.Feedback> */}
              </Form.Group>

              <Form.Group controlId="formBasicPassword" className="mb-5">
                <Form.Label>Confirm New Password</Form.Label>
                <InputGroup hasValidation>
                  <InputGroup.Prepend>
                    <InputGroup.Text><i class="fas fa-unlock-alt"></i></InputGroup.Text>
                  </InputGroup.Prepend>
                  <Form.Control type="password" name="repassword" value={passwords.rePassword} onKeyPress={handleKeyPress} onChange={onChangeHandler} placeholder="Re-Enter Password" />
                </InputGroup>
              </Form.Group>

              <Form.Group className="text-center">
                <Button
                  onClick={changePassword}
                  className={classes.root}
                // ref={loginRef}
                >Submit</Button>
              </Form.Group>
            </Form>
            {/* </div> */}
          </CardBody>
        </Card>
        {/* <Row className="mt-3">
          <Col xs="6">
            <a
              className="text-light"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <small>Forgot password?</small>
            </a>
          </Col>
          <Col className="text-right" xs="6">
            <a
              className="text-light"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <small>Create new account</small>
            </a>
          </Col>
        </Row> */}
      </Col>
      </Row>
    </div>
      
    </>
  );
};

const mapStateToProps = state => {
  return {
    loading: state.login.loading,
    user_id: state.login.data.user_id
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logout: ()=>{dispatch(logout())},
    loginRequest: () => { dispatch(loginUserRequest()) },
    loginSuccess: (data) => { dispatch(loginUserSuccess(data)) },
    loginFailure: (error) => { dispatch(loginUserFailure(error)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
