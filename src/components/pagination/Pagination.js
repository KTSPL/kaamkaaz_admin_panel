import React from 'react';
import { Row,Button } from 'reactstrap';

const Pagination = ({nextPage,previousPage,pageNo,name,length,nextPageStatus}) => {
    return (
        <>
        <Row className="pagination mt-2 mb-5 w3-animate-bottom">
            <Button className="mr-3 "
                disabled={pageNo === 0 ? true : false}
                onClick={()=>{previousPage(name)}}>&lt;</Button>
            <Button 
            // disabled={length<10}
            disabled={nextPageStatus&&length<10}
            onClick={()=>{nextPage(name)}}>&gt;</Button>
        </Row>
        </>
    );
};

export default Pagination;