/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { connect } from "react-redux";
import { Link, useHistory } from "react-router-dom";

// reactstrap components


import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Navbar,
  Nav,
  Container,
  Media,
} from "reactstrap";
import { logout } from "redux/login/login.action";

const AdminNavbar = (props) => {
  const {logout,roleId,user_name} = props
  let history= useHistory()
  return (
    <>
      <Navbar  className="navbar-top navbar-dark" style={{
        height: "64px",
        position: "absolute",
      }} expand="md" id="navbar-main">
        <Container fluid>
          {/* <font
            className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block"
            to="/"
          >
            {props.brandText}
          </font> */}
         
          <Nav className="align-items-center d-none d-md-flex ml-auto" navbar>
          <div className="kk-dropdown ml-auto">
            <div className="kk-dropbtn pointer">
              <Media className="align-items-center">
                <span className="avatar avatar-sm rounded-circle">
                  <img
                    alt="..."
                    src={
                      require("../../assets/img/KTSPLLogo.png")
                        .default
                    }
                  />
                </span>
                <Media className="ml-2 d-none d-lg-block">
                  <span className="mb-0 text-sm font-weight-bold">
                    {
                      roleId===1?"Super Admin":"Employer"
                    }
                    </span>
                </Media>
              </Media>
            </div>
            <div class="kk-dropdown-content">
              {/* <a href="#"
              onClick={()=>{
                history.push('/admin/profile')
              }}
              ><i class="fas fa-user-alt mr-3"></i>My profile</a>
              <a href="#"><i class="fas fa-cog mr-3"></i>Settings</a> */}
              <a href="#" 
              onClick={logout}
              ><i class="fas fa-sign-out-alt mr-3"></i>Logout</a>
              <a style={{cursor:"pointer"}} 
              onClick={()=>{
                if(roleId==1){
                  history.push('/admin/changepassword')
                }else{
                  logout()
                  setTimeout(()=>{
                    window.location.href=`https://kaamkaaz-back-end-server.herokuapp.com/change_password.html?email=${user_name}`
                  },500)
                  
                }
              }}
              ><i class="fas fa-sign-out-alt mr-3"></i>Change Password</a>
            </div>
          </div>
          </Nav>
          {/* <Nav className="align-items-center d-none d-md-flex ml-auto" navbar>
            <UncontrolledDropdown nav>
              <DropdownToggle className="pr-0" nav>
                <Media className="align-items-center">
                  <span className="avatar avatar-sm rounded-circle">
                    <img
                      alt="..."
                      src={
                        require("../../assets/img/KTSPLLogo.png")
                          .default
                      }
                    />
                  </span>
                  <Media className="ml-2 d-none d-lg-block">
                    <span className="mb-0 text-sm font-weight-bold">
                      Super Admin
                    </span>
                  </Media>
                </Media>
              </DropdownToggle>
              <DropdownMenu className="dropdown-menu-arrow" right>
                <DropdownItem to="/admin/user-profile" tag={Link}>
                  <i className="ni ni-single-02" />
                  <span>My profile</span>
                </DropdownItem>
                <DropdownItem to="/admin/user-profile" tag={Link}>
                  <i className="ni ni-settings-gear-65" />
                  <span>Settings</span>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem href="#pablo" onClick={(e) => e.preventDefault()}>
                  <i className="ni ni-user-run" />
                  <span>Logout</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav> */}
        </Container>
      </Navbar>
    </>
  );
};

const mapDispatchToProps = dispatch =>{
  return{
    logout: ()=>{dispatch(logout())}
  }
}

const mapStateToProps = state =>{
  return {
    roleId : state.login.data.role_id,
    user_name: state.login.data.user_name
  }
}
 

export default connect(mapStateToProps,mapDispatchToProps)(AdminNavbar);
