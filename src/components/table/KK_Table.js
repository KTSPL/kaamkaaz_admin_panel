import React, { useEffect, useState } from 'react';
import {
	Card, CardBody, CardHeader, Col, Row,
	Badge,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	Media,
	Progress,
	Table,
	Button,
	UncontrolledTooltip
} from 'reactstrap';

const KK_Table = ({ tableData,actions, actionArray  }) => {
	// const numberOftd = tableHeaders.length
const [operatingData,setOperatingData] = useState(null)

	const generateThs = () =>{
		let res = tableData.filter((data,idx)=>idx<1).map(data=>{
			let values = Object.getOwnPropertyNames(data)
			return(
					values.map(item=>{
						return(
							<th scope="col">{item}</th>
						)
					})
			)
		})
		return res
	}

	const generateTds = () => {
		//    let tdList=[]
		//    let res= []
		let res = tableData.map(data => {
			let values = Object.values(data)
			return (
				<tr>{
					values.map(item => {
						return (
							<>
							<td scope="col">{item}</td>
										</>
						)
					})}
					<UncontrolledDropdown>
							<DropdownToggle
								className="btn-icon-only text-light"
								// href="#pablo"
								role="button"
								size="sm"
								color=""
								onClick={e => e.preventDefault()}
							>
								<i className="fas fa-ellipsis-v" />
							</DropdownToggle>
							<DropdownMenu className="dropdown-menu-arrow" right>
					{
						actionArray.map(item=>{
							return(
								<DropdownItem
									href="#pablo"
									name="DELETE"
									onClick={item}
								>
									Delete
										</DropdownItem>
							)
						})
					}
					</DropdownMenu>
					</UncontrolledDropdown>
				</tr>
				
			)

		})
		return res
	}
	
	return (
		<div className='table-responsive'>
			<Table className="align-items-center table-condensed table-striped mt-6 w3-animate-bottom"
				style={{
					marginLeft: "auto",
					marginRight: "auto",
					width: "550px",
					backgroundColor: "white",
				}}
				responsive>
				<thead class="thead-dark">
					<tr>
						{
							// data.map()
							generateThs()
							// (tableHeaders) ? tableHeaders.map(header => (<th scope="col">{header}</th>)) : ""

						}
					</tr>
				</thead>
				<tbody>

					{
						generateTds()
					}
				</tbody>
			</Table>
		</div >
	)
};

export default KK_Table;




