export const validation = {
  onlyLettersCheck(testValue, testFieldName) {
    if (testValue.match("^[a-zA-Z0-9]*$") != null) {
      return { validation: true };
    } else {
      return {
        validation: false,
        errMsg: "Only Letters are allowed",
      };
    }
  },

  
  onlyNumberCheck(testValue) {
    if (testValue.match("^[0-9]{10,12}$") != null) {
      return { validation: true };
    } else {
      return {
        validation: false,
        errMsg: "Enter valid phone number",
      };
    }
  },
  
  spacesCheck(testValue) {
    if (testValue.match("^[\S]+$") != null) {
      return { validation: false };
    } else {
      return {
        validation: true,
        
      };
    }
  },

  checkForEmpty(testValue, testFieldName) {
    console.log(testFieldName)
    console.log(testValue && testValue !== null)
    console.log(testValue && testValue !== undefined)
    console.log(testValue && testValue !== null && testValue&&testValue.length !== undefined)
    if (testValue && testValue !== null || testValue&&testValue.length !== undefined) {
      return { validation: true };
    } else {
      return {
        validation: false,
        errMsg: `${testFieldName} is Required`,
      };
    }
  },

  emailCheck(testValue) {
    if (testValue.match("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$") != null) {
      return { validation: true };
    } else {
      return {
        validation: false,
        errMsg: "Enter Valid E-mail",
      };
    }
  },
};
