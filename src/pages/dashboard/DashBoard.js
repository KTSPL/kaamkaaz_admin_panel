/* eslint-disable react-hooks/exhaustive-deps */
import CountTag from 'components/count_tag/CountTag';
import React, { useEffect, useState } from 'react';
import { Row, Col, Card, CardBody, CardFooter, Navbar } from 'reactstrap'
import Chart from "chart.js";
import { Line, Bar } from "react-chartjs-2";
import {
	chartOptions,
	parseOptions,
	chartExample2,
	chartExample1
} from "variables/charts.js";
import './dashboard.scss'
import { useSelector } from 'react-redux';
import apis from 'apis';

const DashBoard = () => {
	const auth_token = useSelector(state => state.login.data.auth_token)
	const [empCount, setEmpCount] = useState(null);
	const [jobCount, setJobCount] = useState(null);
	const [candidateCount, setCandidateCount] = useState(0)
	const [appliedCount, setAppliedCount] = useState(null)
	const [monthWiseEmpCount, setMonthWiseEmpCount] = useState([]);
	const [monthWiseAppliedCount, setMonthWiseAppliedCount] = useState([]);
	const [monthWiseAppliedCountX,setMonthWiseAppliedCountX] = useState([])
	const [jobCountYear, setJobCountYear] = useState(null)
	const [monthWiseJobCountArray, setMonthWiseJobCountArray] = useState([])
	const role_Id = useSelector(state => state.login.data.role_id)

	useEffect(() => {
		console.log("LATEST JOB COUNT", jobCount)
	}, [jobCount])

	let stateD = {
		labels: monthWiseAppliedCount && monthWiseAppliedCount.map(item => item.month.slice(0, 3)),
		datasets: [
			{
				label: 'Applied Candidates',

				backgroundColor: '#ff5722',
				hoverBackgroundColor: [
					'#4B5000',
					'#175000',
					'#003350',

				],
				fill: false,
				data: monthWiseAppliedCountX,
				barThickness: 20

			}
		]
	}
	let stateL = {
		labels: monthWiseJobCountArray && monthWiseJobCountArray.map(item => item.month.slice(0, 3)),
		datasets: [
			{
				label: 'Jobs',
				backgroundColor: [
					'#ff1744',
					'#ffff8d',
					'#69f0ae',
					'#0091ea',
					'#7c4dff'
				],
				hoverBackgroundColor: [
					'#501800',
					'#4B5000',
					'#175000',
					'#003350',
					'#35014F'
				],
				fill: false,
				data: monthWiseJobCountArray && monthWiseJobCountArray.map(item => item.jobCount),
				borderColor: '#3f51b5',
				pointBackgroundColor: 'blue',

			}
		]
	}



	useEffect(async () => {
		if (window.Chart) {
			parseOptions(Chart, chartOptions());
		}
		try {
			let res1
			let res3
			if (role_Id === 1) {
				res1 = await apis.masters.getAllRecords(auth_token, "/api/v1/dashboard/employer_count")
			}

			// let res2 = await apis.masters.getAllRecords(auth_token,"/api/v1/dashboard/job_count")

			let jobPostedCountRes = await apis.masters.getAllRecords(auth_token, '/api/v1/master/postJob_count')
			let appliedCandidateCountRes = await apis.masters.getAllRecords(auth_token, '/api/v1/master/applied_candidate_count')
			let monthWiseJobCountRes = await apis.masters.getAllRecords(auth_token, '/api/v1/master/postJob_monthwise_count')
			let monthWiseAppliedCandidateCount = await apis.masters.getAllRecords(auth_token, '/api/v1/master/postJob_monthwise_appliedcount')
			console.log("Month-Wise Job Count", monthWiseJobCountRes)
			if (appliedCandidateCountRes.success) {
				setAppliedCount(appliedCandidateCountRes.data[0].appliedCandidateCount)
			}

			if (monthWiseJobCountRes.success) {
				console.log(monthWiseJobCountRes.data)
				let newArray = monthWiseJobCountRes.data.map(i => ({ jobCount: i.jobCount, month: i.month }))
				setJobCountYear(monthWiseJobCountRes.data[0].Year)
				setMonthWiseJobCountArray(newArray)
			}
			if (monthWiseAppliedCandidateCount.success) {
				let newArray = monthWiseAppliedCandidateCount.data.map(i => ({ appliedCandidateCount: i.appliedCandidateCount, month: i.month }))
				setMonthWiseAppliedCount(newArray)
				let xAxis=newArray.map(item=>item.appliedCandidateCount)
				setMonthWiseAppliedCountX(xAxis) 
			}
			console.log(jobPostedCountRes.success)
			if (jobPostedCountRes.success) {
				console.log("CONTROL IS IN JOB POST SETTING")
				setJobCount(jobPostedCountRes.data[0].JobCount)
			}

			let candidateCountRes = await apis.counts.count('/candidateCount')


			if (role_Id === 1) {
				res3 = await apis.masters.getAllRecords(auth_token, "/api/v1/dashboard/month_wise_employer_count")
			}

			if (role_Id === 1) {
				if (res1.success) {
					setEmpCount(res1.data.employer_count)
				}
			}




			if (candidateCountRes.resultCode === 1) {
				setCandidateCount(candidateCountRes.resultData[0].candidateCount)
			}

			if (res3.success) {
				setMonthWiseEmpCount(res3.data)
			}
		}

		catch (err) {
			console.error(err)
		}

	}, [])

	// useEffect(()=>{
	//  console.log(a);
	// },[a])

	return (
		<div className="content dashBoardClass  mt-6" >
			<div className="tags" style={{ width: "100%", display: "flex", justifyContent: "space-around", flexWrap: "wrap", position: "relative" }} >
				{/* <Col lg="3" sm="6" md="6" className="d-flex justify-content-center"> */}
				<CountTag className="mt-2" dataCount={empCount} header="Employers" iconLink={"fas fa-user-tie fa-2x"} color="#f5365c" />
				{/* </Col> */}
				{/* <Col lg="3" sm="6" md="6" className="d-flex justify-content-center"> */}
				<CountTag className="mt-2" dataCount={candidateCount} header="Candidates" iconLink={"fas fa-users fa-2x"} color="#673ab7" />
				{/* </Col> */}
				{/* <Col lg="3" sm="6" md="6" className="d-flex justify-content-center"> */}
				<CountTag className="mt-2" dataCount={appliedCount} header="Jobs Applied" iconLink={"fas fa-user-tie fa-2x"} color="#009688" />
				{/* </Col> */}
				{/* <Col lg="3" sm="6" md="6" className="d-flex justify-content-center"> */}
				<CountTag className="mt-2" dataCount={jobCount} header="Jobs Posted" iconLink={"fas fa-briefcase fa-2x"} color="#d500f9" />
				{/* </Col> */}
			</div>
			<Row className="d-flex justify-content-center mt-5" style={{ width: "100%", height: "100%", marginLeft: "auto", marginRight: "auto" }}>
				<Col lg="6" className="d-flex justify-content-center mt-2 " >
					<Card className="box-shadow chart-card" style={{ width: "100%" }}>
						<CardBody>
							<div className="chart">
								<Bar
									data={stateD}
									options={{
										layout: {
											padding: {
												left: -5,
												right: 0,
												top: 0,
												bottom: -8
											}
										},
										responsive: true,
										maintainAspectRatio: false,
										title: {
											display: true,
											text: `Applied Jobs(${jobCountYear})`,
											fontSize: 15
										},
										scales: {
											xAxes: [{
												gridLines: {
													display: false,
													color: 'rgba(255, 0, 0, .2)', // can still set the colour.

												},
											}],
											yAxes: [{
												gridLines: {
													display: false
												},
												// scaleLabel: {
												//   display: true,
												//   labelString: 'Revenue'
												// },
												ticks: {
													suggestedMin: 5000000,
													lineTension: 0,

												}
											}]
										},
									}}
								/>
							</div>
						</CardBody>
					</Card>
				</Col>
				<Col lg="6" className="d-flex justify-content-center mt-2">
					<Card className="bg-default box-shadow chart-card" style={{ width: "100%" }}>
						<CardBody>
							<div className="chart">
								{/* Chart wrapper */}
								{/* <Line
									data={chartExample1.data1}
									options={chartExample1.options}
									getDatasetAtEvent={e => console.log(e)}
								/> */}
								<Line
									data={stateL}
									options={{
										layout: {
											padding: {
												left: -5,
												right: 0,
												top: 0,
												bottom: -8
											}
										},
										responsive: true,
										maintainAspectRatio: false,
										title: {
											display: true,
											text: `Total Jobs(${jobCountYear})`,
											fontSize: 15
										},
										scales: {
											xAxes: [{
												gridLines: {
													display: false,
													color: 'rgba(255, 0, 0, .2)', // can still set the colour.

												},
											}],
											yAxes: [{
												gridLines: {
													display: false,
													color: '#84ffff77'
												},

												ticks: {
													suggestedMin: 5000000,
													lineTension: 0,

												}
											}]
										},
										legend: {
											display: false,

										}

									}}
								/>
							</div>
						</CardBody>
					</Card>
				</Col>
			</Row>
		</div>
	);
};

export default DashBoard;