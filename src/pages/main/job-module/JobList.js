/* eslint-disable react-hooks/exhaustive-deps */
import Pagination from '../../../components/pagination/Pagination'
import apis from 'apis';
import KK_Toast from 'components/Toast/KK_Toast';
import { scrollToTop } from 'components/_helpers/utils';
import React, { useEffect, useState } from 'react';
import { connect, useSelector } from 'react-redux';
import { useHistory } from 'react-router';

import {
	Card,
	CardBody,
	CardHeader,
	Col, Row,
	Badge,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	Media,
	Progress,
	Table,
	Button,
	UncontrolledTooltip,
	PaginationItem,
	PaginationLink,
	Spinner
} from 'reactstrap';
import { decrementPageNo } from 'redux/pagination/pagination.action';
import { incrementPageNo } from 'redux/pagination/pagination.action';
import EmptyTablePlaceHolder from 'components/EmptyTableReplacement.js/EmptyTablePlaceHolder';
import { logout } from 'redux/login/login.action';

const JobList = ({ setJobData, setToastConfig, setTab, setWorkFlow, pageNo, nextPage, previousPage, auth_token, logout }) => {
	console.log("pageNo", pageNo)
	const [deleteId, setDeleteId] = useState()
	const [editId, setEditId] = useState()
	const [tableData, setTableData] = useState()
	const [page_no, setPageNo] = useState(0)
	const [loader,setLoader] = useState(false)
	const [nextPageStatus, setNextPageStatus] = useState(false)
	const role_id = useSelector(state => state.login.data.role_id);
	let history = useHistory()

	useEffect( async () => {
			try {
				let res = await apis.masters.getAllRecords(auth_token, `/api/v1/master/postJob/${pageNo === 0 ? "" : pageNo}`)
			
				if(res.success===false&&res.error_code){
					console.log("Here is the error")
					setToastConfig({
						toastStatus: true,
						message: res.message,
						severity: "error"
					})
				  return setTimeout(()=>{
					logout()
				   },2000)
				}
				console.log(res.data.length>0)
				if (res.data.length > 0) {
					setLoader(false)
					setNextPageStatus(true)
				} else {
					setLoader(false)
					setNextPageStatus(false)
				}
			} catch (err) {
				setLoader(false)
				if(err.response&&err.response&&err.response.status===401){
					setToastConfig({
						toastStatus:true,
						message: 'Session Expired!',
						severity:'error'
					})
					setTimeout(()=>{
						logout()
					},2000)
				}
				console.log("ERROR MEIN AAYA",err)
			}
		getAllRecords()
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	useEffect(() => {
		getAllRecords()
	}, [pageNo])


	useEffect( () => {
		console.log(deleteId)
		async function DeleteRecord(){
			if (deleteId) {
				try {
					let res = await apis.masters.deleteRecordRequest(auth_token, '/api/v1/master/postJob', deleteId)
					if (res.success) {
						scrollToTop()
						setToastConfig({
							toastStatus: true,
							message: res.message,
							severity: "success"
						})
						return getAllRecords()
					} else {
						scrollToTop()
						return setToastConfig({
							toastStatus: true,
							message: res.message,
							severity: "error"
						})
					}
				}
				catch (err) {
					scrollToTop()
					if(err.response&&err.response.status===401){
						setToastConfig({
							toastStatus:true,
							message: 'Session Expired!',
							severity:'error'
						})
						setTimeout(()=>{
							logout()
						},2000)
					}
					setToastConfig({
						toastStatus: true,
						message: err.message,
						severity: "error"
					})
					console.error(err)
				}
			}
		}
		DeleteRecord()
	}, [deleteId])


	useEffect(() => {
		if (editId) {
			history.push({
				pathname: "/admin/addJob/" + editId
			})
		}
	}, [editId])


	const generateThs = () => {
		let res = tableData && tableData.filter((data, idx) => idx < 1).map(data => {
			let values = Object.getOwnPropertyNames(data)
			return (
				values.map(item => {
					return (
						<th className='text-white' scope="col">{item}</th>
					)
				})
			)
		})
		return res
	}


	const generateTds = () => {
		//    let tdList=[]
		//    let res= []
		let res = tableData && tableData.map(data => {
			let values = Object.values(data)
			return (
				<tr>{
					values.map(item => {
						return (
							<>
								<td >{item}</td>
							</>
						)
					})}
					<td className="text-right">
						<UncontrolledDropdown>
							<DropdownToggle
								className="btn-icon-only text-light"
								// href="#pablo"
								role="button"
								size="sm"
								color=""
								onClick={e => e.preventDefault()}
							>
								<i className="fas fa-ellipsis-v" />
							</DropdownToggle>
							<DropdownMenu className="dropdown-menu-arrow" right>
								{
									!(role_id===1)&&<DropdownItem
									name="EDIT"
									onClick={(e) => {
										setJobData(data)
										setWorkFlow("EDIT")
										setTab("ADD")
									}}
								>
									Edit
										</DropdownItem>

								}
								
								<DropdownItem
									name="DELETE"
									onClick={(e) => {
										setDeleteId(data.id);
									}}
								>
									Delete
										</DropdownItem>
							</DropdownMenu>
						</UncontrolledDropdown>
					</td>
				</tr>

			)

		})
		return res
	}

	const getAllRecords = async () => {
		setLoader(true)
		try {
			let res = await apis.masters.getAllRecords(auth_token, `/api/v1/master/postJob/${pageNo === 0 ? "" : pageNo}`)
			if(res.success===false&&res.error_code){
				console.log("Here is the error")
				setToastConfig({
					toastStatus: true,
					message: res.message,
					severity: "error"
				})
			   setTimeout(()=>{
				logout()
			   },2000)
			}
			if (res.success) {
				setLoader(false)
				setTableData(res.data)
			}
		}
		catch (err) {
			setLoader(false)
			if(err.response&&err.response&&err.response.status===401){
				setToastConfig({
					toastStatus:true,
					message: 'Session Expired!',
					severity:'error'
				})
				setTimeout(()=>{
					logout()
				},2000)
			}
			console.error(err)
		}
	}

	
	return (
		<>
			{/* Table Code */}
		{
			loader?
			<Spinner style={{ width: '3rem', height: '3rem', color:"whitesmoke" }}  />
			:
			(tableData&&tableData.length>0)?
			<>
			<Table className="align-items-center table-condensed table-striped  w3-animate-bottom"
			style={{
				marginLeft: "auto",
				marginRight: "auto",
				backgroundColor: "white",
			}}
			responsive>
			<thead className="thead-dark"
			>
				{
					generateThs()
				}
				<th className='text-white' scope="col">Actions</th>
			</thead>
			<tbody>
				{
					generateTds()
				}
			</tbody>
		</Table>
		<Pagination nextPageStatus={nextPageStatus} length={tableData && tableData.length} nextPage={nextPage} name="jobPageNo" previousPage={previousPage} pageNo={pageNo} />
		</>
		:
		<EmptyTablePlaceHolder>
			No Jobs Found
		</EmptyTablePlaceHolder>
		}	
		</>
	);
};


const mapStateToProps = state => {
	return {
		pageNo: state.pagination.jobPageNo,
		auth_token: state.login.data.auth_token
	}
}

const mapDispatchToProps = dispatch => {
	return {
		nextPage: (item) => { dispatch(incrementPageNo(item)) },
		previousPage: (item) => { dispatch(decrementPageNo(item)) },
		logout : ()=> {dispatch(logout())}
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(JobList);