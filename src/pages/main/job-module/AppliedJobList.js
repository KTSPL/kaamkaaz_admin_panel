/* eslint-disable react-hooks/exhaustive-deps */
import apis from 'apis';
import SelectComponent from 'components/customSelect/Select.Component';
import EmptyTablePlaceHolder from 'components/EmptyTableReplacement.js/EmptyTablePlaceHolder';
import KK_Toast from 'components/Toast/KK_Toast';
import React, { useEffect, useState } from 'react';
import { connect, useSelector } from 'react-redux';
import {
	Col, Row, Table, DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle
} from 'reactstrap';
import { logout } from 'redux/login/login.action';

const AppliedJobList = ({ authToken }) => {
	const [toastConfig, setToastConfig] = useState({
		toastStatus: false,
		message: "",
		severity: null,
	})
	const [jobList, setJobList] = useState([])
	const [selectedJobId, setSelectedJobId] = useState("");
	const [tab, setTab] = useState("Applied")
	const [activeStyle, setActiveTabStyle] = useState({
		Appliedstyle: 'active-tab'
	})
	const [activeBtnStyle, setActiveBtnStyle] = useState({
		Appliedstyle: 'bounce-1'
	})
	const [candidateList, setCandidateList] = useState([])
	const [jobId, setJobId] = useState(null);
	const [status, setStatus] = useState(1);
	const auth_token = useSelector(state => state.login.data.auth_token)

	useEffect(async () => {
		try {
			let list = await apis.masters.getAllRecords(authToken, '/api/v1/master/postJob')
			console.log("list", list)
			let jobList = list.data.map(item => {
				return {
					id: item.id,
					job_title: item.job_title
				}
			})
			setJobList(jobList)
		}
		catch (err) {
			if (err.response && err.response && err.response.status === 401) {
				setToastConfig({
					toastStatus: true,
					message: 'Session Expired!',
					severity: 'error'
				})
				setTimeout(() => {
					logout()
				}, 2000)
			}
			console.log(err)
		}
		getCandidates()
	}, [])

	const onChangeHandler = (e) => {
		setJobId(e.target.value)
	}

	const selectOrRejectCandidate = async (job_application_id, status) => {
		let body = {
			job_application_id,
			process_status: status
		}
		console.log("BODY", body, auth_token)
		try {
			let res = await apis.candidates.selectOrRejectCandidateApplication(auth_token, body)
			if (res.success) {
				setToastConfig({
					toastStatus: true,
					message: res.message,
					severity: 'success'
				})
				getCandidates()
			}
		} catch (err) {
			if (err.response && err.response && err.response.status === 401) {
				setToastConfig({
					toastStatus: true,
					message: 'Session Expired!',
					severity: 'error'
				})
				setTimeout(() => {
					logout()
				}, 2000)
			} else {
				setToastConfig({
					toastStatus: true,
					message: "Something Went Wrong!",
					severity: 'error'
				})
			}
		}
	}

	const getCandidates = async () => {
		try {
			let res = await apis.candidates.getCandidatesWithProcessStatus(auth_token, jobId, status)
			console.log(res)
			if (res.success) {
				console.log("list", res.data)
				setCandidateList(res.data)
			} else {
				setToastConfig({
					toastStatus: true,
					message: res.message,
					severity: "error"
				})
				setCandidateList([])
			}
		} catch (err) {
			if (err.response && err.response.status === 401) {
				setToastConfig({
					toastStatus: true,
					message: 'Session Expired!',
					severity: 'error'
				})
				setTimeout(() => {
					logout()
				}, 2000)
			}
			console.error(err.response);
		}
	}
	useEffect(() => {
		console.log(candidateList)
	}, [candidateList])

	const onActive = (e) => {
		setActiveTabStyle({
			[e.target.name + "style"]: "active-tab"
		})
		setActiveBtnStyle({
			[e.target.name + "style"]: "bounce-1"
		})
		setTab(e.target.name)
		if (e.target.name === "Applied") {
			setStatus(1)
		}
		if (e.target.name === "Selected") {
			setStatus(2)
		}
		if (e.target.name === "Rejected") {
			setStatus(3)
		}
	}

	useEffect(() => {
		console.log(jobId, status)
		getCandidates()
	}, [status, jobId])
	// useEffect(()=>{
	//     if(tab==="Applied"){

	//     }
	// },[tab])

	const candidateListdummy = [
		{
			id: 1,
			name: "Rishabh Mishra",
			location: "Pune",
			exp: "4",
			skills: "javascript,java,c++",
			job: "Web Dev"
		},
		{
			id: 2,
			name: "Sanchit Gupta",
			location: "Kanpur",
			exp: "3",
			skills: "java,c++,C,Flutter,Android",
			job: "Android Developer"
		},
		{
			id: 3,
			name: "Prabhakar Awasthi",
			location: "Pune",
			exp: "4",
			skills: "Trading,marketting",
			job: "Business Analyst"
		},
		{
			id: 4,
			name: "Ayushman Gupta",
			location: "Kanpur",
			exp: "3",
			skills: "MBBS",
			job: "Doctor"
		},
		{
			id: 5,
			name: "Anurag Mishra",
			location: "Noida",
			exp: "2",
			skills: "Sql Developer",
			job: "Database Administrator"
		},
		{
			id: 6,
			name: "Akshat Rastogi",
			location: "Meerut",
			exp: "3",
			skills: "Python,C,java",
			job: "AI developer"
		}
	]

	const generateThs = () => {
		let res = (candidateList.length > 0) && candidateList.filter((data, idx) => idx < 1).map(data => {
			let values = Object.getOwnPropertyNames(data)
			return (

				values.map(item => {
					return (
						<th className='text-white' scope="col">{item}</th>
					)
				})
			)
		})
		return res
	}

	const generateTds = () => {
		//    let tdList=[]
		//    let res= []
		let res = (candidateList.length > 0) && candidateList.map(data => {
			let values = Object.values(data)
			return (
				<tr>
					{
						(status === 2) ? <td className="text-right"><i className="fas fa-circle text-green"></i></td> : null
					}
					{
						(status === 3) ? <td className="text-right"><i className="fas fa-circle text-red"></i></td> : null
					}


					{

						values.map(item => {
							return (
								<>
									<td scope="col">{item}</td>
								</>
							)
						})}
					{
						(status === 1) && <td className="text-right">
							<UncontrolledDropdown>
								<DropdownToggle
									className="btn-icon-only text-light"
									// href="#pablo"
									role="button"
									size="sm"
									color=""
									onClick={e => e.preventDefault()}
								>
									<i className="fas fa-ellipsis-v" />
								</DropdownToggle>
								<DropdownMenu className="dropdown-menu-arrow" right>
									<DropdownItem
										name="Select"
										onClick={(e) => {
											selectOrRejectCandidate(data.application_id, 2)
										}}
									>
										<i className="fas fa-circle text-green"></i>
										SELECT
									</DropdownItem>
									<DropdownItem
										name="Reject"
										onClick={(e) => {
											selectOrRejectCandidate(data.application_id, 3)
										}}
									>
										<i className="fas fa-circle text-red"></i>
										REJECT
									</DropdownItem>
								</DropdownMenu>
							</UncontrolledDropdown>
						</td>
					}

				</tr>

			)

		})
		return res
	}


	return (
		<div className="pr-2 pl-2">
			<Row className="mt-8 text-center w3-animate-top">
				<Col>
					<KK_Toast setToastConfig={setToastConfig} toastConfig={toastConfig} />
				</Col>
			</Row>
			<Row className="mt-3">
				<Col xl="4" className={`d-flex justify-content-center tab-border ${activeStyle.Appliedstyle} `}><button className={`tab-style ${activeBtnStyle.Appliedstyle}`} onClick={onActive} name="Applied">{('Applied Candidates').toLocaleUpperCase()}</button></Col>
				<Col xl="4" className={`d-flex justify-content-center tab-border ${activeStyle.Rejectedstyle}`}><button className={`tab-style ${activeBtnStyle.Rejectedstyle}`} onClick={onActive} name="Rejected">{('Rejected Candidates').toLocaleUpperCase()}</button></Col>
				<Col xl="4" className={`d-flex justify-content-center tab-border ${activeStyle.Selectedstyle}`}><button className={`tab-style ${activeBtnStyle.Selectedstyle}`} onClick={onActive} name="Selected">{('Selected Candidates').toLocaleUpperCase()}</button></Col>
				{/* <Col className={`d-flex justify-content-center tab-border`}><button name="Tab4">Tab4</button></Col> */}
			</Row>
			<Row className="mt-6 text-center w3-animate-top" >
				<Col xl="4"></Col>
				<Col xl="4" className="d-flex justify-content-center " >
					<SelectComponent name="job_category" onChange={onChangeHandler} style={{ width: "200px" }}>
						<option>Select Job</option>
						{
							jobList.map((job, idx) => {
								return <option key={idx} value={job.id}
								>{job.job_title}</option>
							})
						}
					</SelectComponent>
				</Col>
				<Col xl="4">

				</Col>
			</Row>

			<Row className="text-center mt-3" >
				<Col xl="12">
					{
						(candidateList && candidateList.length > 0) ? <div className='table-responsive'>
							<Table className="align-items-center table-condensed table-striped mt-3 w3-animate-bottom"
								style={{
									marginLeft: "auto",
									marginRight: "auto",
									// width: "550px",
									backgroundColor: "white",
								}}
								responsive>

								<thead class="thead-dark ">
									{
										(status === 2 || status === 3) &&
										<th className='text-white' scope="col">Indicator</th>
									}
									{
										generateThs()
									}
									{
										(status === 1) && <th className='text-white' scope="col">Actions</th>
									}

								</thead>
								<tbody>
									{
										generateTds()
									}
								</tbody>
							</Table>
						</div>
							:
							<EmptyTablePlaceHolder>
								No Candidates Found
							</EmptyTablePlaceHolder>
					}
				</Col>
			</Row>
		</div>
	);
};

const mapStateToProps = state => {
	return {
		authToken: state.login.data.auth_token
	}
}

const mapDispatchToProps = dispatch => {
	return {
		logout: () => { dispatch(logout()) }
	}
}

export default connect(mapStateToProps, mapDispatchToProps())(AppliedJobList);