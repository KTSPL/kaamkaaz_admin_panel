/* eslint-disable default-case */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import Form from 'react-bootstrap/Form'
import {
	Card, CardBody, CardHeader, Col, Row,
	Badge,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	Media,
	Progress,
	Table,
	Button,
	UncontrolledTooltip,
	Spinner
} from 'reactstrap';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory, useParams } from 'react-router-dom';
import { content } from '../../components/_helpers/Content';
import apis from 'apis';
import KK_Toast from 'components/Toast/KK_Toast';
import KK_Table from 'components/table/KK_Table';
import axios from 'axios';
import { connect, useDispatch, useSelector } from 'react-redux';

import paginationReducer from 'redux/pagination/pagination.reducer';

import { decrementPageNo } from 'redux/pagination/pagination.action';
import Pagination from 'components/pagination/Pagination';
import { incrementPageNo } from 'redux/pagination/pagination.action';
import { logout } from 'redux/login/login.action';




const useStyles = makeStyles({
	root: {
		background: 'linear-gradient(45deg, #7c4dff 30%, #b388ff  90%)',
		border: 0,
		borderRadius: 10,
		boxShadow: '0 3px 5px 2px #b488ff83',
		color: 'white',
		height: 38,
		width: "100%",
		padding: '0 20px',
	},
	disabled: {
		// background: 'linear-gradient(45deg,#b39ddb 30%, #d1c4e9 90%)',
		backgroundColor: "lightgrey",
		border: 0,
		borderRadius: 10,
		// boxShadow: '0 3px 5px 2px grey',
		color: 'white',
		height: 38,
		padding: '0 20px',
	}
});



const Master = ({auth_token,newPage,oldPage,pageNo,logout}) => {
	const [nextPageStatus, setNextPageStatus] = useState(false)
	const classes = useStyles();
	const params = useParams()
	const history = useHistory()
	const pathName = history.location.pathname;
	const [inputClass, setInputClass] = useState("normal")
	const [selectedContent, setSelectedContent] = useState()
	const [tableData, setTableData] = useState([])
	const [enteredValue, setEnteredValue] = useState("")
	const [operableData, setOperableData] = useState("")
	const [toastConfig, setToastConfig] = useState({
		toastStatus: false,
		message: "",
		severity: null,
	})
	const [workFlow, setWorkFlow] = useState("ADD")
	const [enterMax, setMax] = useState(null)
	const [loader,setLoader] = useState(false)

	const scrollToTop = () => {
		window.scrollTo(0, 0);
	}

	const generateThs = () => {
		let res = tableData.filter((data, idx) => idx < 1).map(data => {
			let values = Object.getOwnPropertyNames(data)
			return (
				values.map(item => {
					return (
						<th scope="col">{item}</th>
					)
				})
			)
		})
		return res
	}


	const generateTds = () => {
		//    let tdList=[]
		//    let res= []
		let res = tableData.map(data => {
			let values = Object.values(data)
			return (
				<tr>{
					values.map(item => {
						return (
							<>
								<td scope="col">{item}</td>
							</>
						)
					})}
					<td className="text-right">
						<UncontrolledDropdown>
							<DropdownToggle
								className="btn-icon-only text-light"
								// href="#pablo"
								role="button"
								size="sm"
								color=""
								onClick={e => e.preventDefault()}
							>
								<i className="fas fa-ellipsis-v" />
							</DropdownToggle>
							<DropdownMenu className="dropdown-menu-arrow" right>
								<DropdownItem
									name="EDIT"
									onClick={(e) => {
										setOperableData(data)
										actionHandler(e)
									}}
								>
									Edit
										</DropdownItem>
								<DropdownItem
									name="DELETE"
									onClick={(e) => {
										setOperableData(data)
										setEnteredValue(null)
										setWorkFlow("DELETE")
									}}
								>
									Delete
										</DropdownItem>
							</DropdownMenu>
						</UncontrolledDropdown>
					</td>
				</tr>

			)

		})
		return res
	}

	// component did mount

	useEffect( async () => {
		setLoader(true)
		let filteredContent = content.find(item => {
			return item.path === pathName
		})
	
		setSelectedContent(filteredContent)
		console.log(filteredContent)
		try {
			let res = await apis.masters.getAllRecords(auth_token, filteredContent.reqpath)
			if (res.data.length>0) {
				setLoader(false)
				setNextPageStatus(true)
			}else{
				setLoader(false)
				setNextPageStatus(false)
			}
		}
		catch (err) {
			setLoader(false)
			if(err.response&&err.response.status===401){
				setToastConfig({
					toastStatus:true,
					message: 'Session Expired!',
					severity:'error'
				})
				setTimeout(()=>{
					logout()
				},2000)
			}
			console.error(err.response)
		}
		//api call to populate the table as per the req in content object		
		getAllRecords(filteredContent.reqpath)
	}, [])

	const getAllRecords = async (path) => {
		setLoader(true)
		console.log(path)
		try {
			let res = await apis.masters.getAllRecords(auth_token,path+`${pageNo===0?"":"/"+pageNo}`)
			console.log("Real Response", res)
			if (res.success === true) {
				setLoader(false)
				setTableData(res.data)
			}
		}
		catch (err) {
			setLoader(false)
			if(err.response&&err.response.status===401){
				setToastConfig({
					toastStatus:true,
					message: 'Session Expired!',
					severity:'error'
				})
				setTimeout(()=>{
					logout()
				},2000)
			}
			console.error(err)
		}
	}

	// const createNewRecord = (path) => {
	// 	apis.masters.createNewRecord(path).then(res => {
	// 		console.log(res)
	// 	}).catch(err => {
	// 		console.log(err)
	// 	})
	// }

	useEffect(()=>{
		console.log("CLASS",inputClass)
	},[inputClass])

	useEffect(() => {
		console.log("tableData", tableData)
	}, [tableData])

	const onChangeHandler = (e) => {
		setInputClass("normal")
		if (e.target.name === "max") {
			setMax(e.target.value)
		} else {
			setEnteredValue(e.target.value)
		}
	}

	useEffect(() => {
		console.log(enteredValue)
	}, [enteredValue])

	useEffect(async() => {
		if(workFlow==="DELETE"){
			
			let body = {
				name: operableData.name,
			}
			console.log("Body",body)
			// scrollToTop()
			try {
				let res = await apis.masters.deleteRecordRequest(auth_token,selectedContent && selectedContent.reqpath,operableData.id, body)
				if (res.success === true) {
					console.log(res)
					setToastConfig({
						toastStatus: true,
						message: res.message,
						severity: "success"
					})
					setWorkFlow("ADD")
					return getAllRecords(selectedContent && selectedContent.reqpath)
				}
				else {
					setToastConfig({
						toastStatus: true,
						message: res.message,
						severity: "error"
					})
					setWorkFlow("ADD")
				}
			}catch (err) {
				if(err.response&&err.response.status===401){
					setToastConfig({
						toastStatus:true,
						message: 'Session Expired!',
						severity:'error'
					})
					setTimeout(()=>{
						logout()
					},2000)
				}
				setToastConfig({
					toastStatus: true,
					message: err.message,
					severity: "error"
				})
				setWorkFlow("ADD")
			 return	console.error(err)
			}
		}
		console.log("workFlow",workFlow)
	}, [workFlow])

	const actionHandler = async (e) => {
		switch (e.target.name) {
			case "ADD": {
				if (enteredValue===undefined||enteredValue.length === 0) {
					return setInputClass("error")
				}
				else{
					let data=null
					if (selectedContent && selectedContent.id === 2) {
						data = {
							min_exp: parseInt(enteredValue),
							max_exp: parseInt(enterMax)
						}
					} else {
						data = {
							name: enteredValue
						}
					}
	
					console.log(data, "DATA");
					// scrollToTop()
					if (workFlow === "EDIT") {
						try {
							let res = await apis.masters.updateRecordRequest(auth_token,selectedContent && selectedContent.reqpath, operableData.id, data)
							if (res.success === true) {
								console.log("Update res", res)
								setToastConfig({
									toastStatus: true,
									message: res.message,
									severity: "success"
								})
								setWorkFlow("ADD")
								setMax("")
								setEnteredValue("")
								setOperableData("")
								return getAllRecords(selectedContent && selectedContent.reqpath)
							} else {
								setToastConfig({
									toastStatus: true,
									message: res.message,
									severity: "error"
								})
							}
						} catch (err) {
							if(err.response&&err.response.status===401){
								setToastConfig({
									toastStatus:true,
									message: 'Session Expired!',
									severity:'error'
								})
								setTimeout(()=>{
									logout()
								},2000)
							}
							setToastConfig({
								toastStatus: true,
								message: err.message,
								severity: "error"
							})
							console.error(err)
						}
					}
					else {
						try {
							let res = await apis.masters.createRecordRequest(auth_token,selectedContent && selectedContent.reqpath, data)
							if (res.success === true) {
								console.log("ADD res", res)
								setToastConfig({
									toastStatus: true,
									message: res.message,
									severity: "success"
								})
								setMax("")
								setEnteredValue("")
								setOperableData("")
								return getAllRecords(selectedContent && selectedContent.reqpath)
							}
						} catch (err) {
							if(err.response&&err.response.status===401){
								setToastConfig({
									toastStatus:true,
									message: 'Session Expired!',
									severity:'error'
								})
								setTimeout(()=>{
									logout()
								},2000)
							}
							console.error(err)
						}
					}
				}
				
				break;
			}
			case "EDIT": {
			return	setWorkFlow("EDIT");
			}
		
			}
	}

	const actionArray = [
		actionHandler,
		setOperableData
	]

	useEffect(() => {
		if(workFlow==="DELETE"){
			setEnteredValue("")
		}else{
			if(selectedContent&&selectedContent.id===2){
				setEnteredValue(operableData.min_exp)
				return setMax(operableData.max_exp)
			}
			setEnteredValue(operableData.name)
		}
		
	}, [operableData])


	useEffect(()=>{
		
		getAllRecords(selectedContent&&selectedContent.reqpath)
	},[pageNo])


	return (
		<>
			<div className="container text-center mt-8 mb-4" style={{ position: "relative" }} >
				<h2 style={{ color: "white" }} className="w3-animate-top">
					{
					workFlow==="EDIT"?`EDIT ${selectedContent && selectedContent.title}`:
						`ADD ${selectedContent && selectedContent.title}`
					}
					</h2>
				<KK_Toast setToastConfig={setToastConfig} toastConfig={toastConfig} />
			</div>

			<div className="d-flex justify-content-center flex-column align-items-center pr-5 pl-5">
				<Col lg="7  w3-animate-left">
					<Card style={{ width: "100%" }}>
						<CardHeader className="pl-4 pt-2 pb-0">
							<Form.Label>
							{
					workFlow==="EDIT"?`Edit ${selectedContent && selectedContent.header}`:
						`Add ${selectedContent && selectedContent.header}`
					}
								</Form.Label>
						</CardHeader>
						<CardBody className="pt-3 pb-0">
							<Row className="p-0">
								<Col xs="12 " sm={selectedContent && selectedContent.id === 2 ? "12 pl-3" : "9 pl-3"} md={selectedContent && selectedContent.id === 2 ? "12 pl-3" : "9 pl-3"} lg={selectedContent && selectedContent.id === 2 ? "12 pl-3" : "9 pl-3"}>
									{
										(selectedContent && selectedContent.id === 2) &&
										<Form.Label>{selectedContent && selectedContent && selectedContent.min}</Form.Label>
									}
									<Form.Group controlId="language">
										<Form.Control type="text"
											onChange={onChangeHandler}
											className={inputClass}
											value={enteredValue}
											placeholder={selectedContent && selectedContent.id === 2 ? selectedContent && selectedContent.min : selectedContent && selectedContent.placeholder} />
										{
											inputClass === "error" && <small className="errText">This Field Can't be Empty</small>
										}
									</Form.Group>
								</Col>
								{
									(selectedContent && selectedContent.id === 2) &&
									<Col xs="12 " sm="12 pl-3" md="12 pl-3" lg={"12 pl-3"}>
										<Form.Label>{selectedContent && selectedContent.max}</Form.Label>
										<Form.Group controlId="language">
											<Form.Control type="text"
												name="max"
												onChange={onChangeHandler}
												className={inputClass}
												value={enterMax}
												placeholder={selectedContent && selectedContent.max} />
												{
											inputClass === "error" && <small className="errText">This Field Can't be Empty</small>
										}
										</Form.Group>
									</Col>
								}

								<Col
									style={selectedContent && selectedContent.id === 2 ?
										{
											left: "50%",
											transform: "translateX(-50%)"
										}
										:
										null}
									xs="12 d-flex justify-content-center pb-3 w-100" sm="3" md="3" lg={selectedContent && selectedContent.id === 2 ? "3 pr-3" : "3 pr-3"}>
									<Button name="ADD"
										className="ripple"
										onClick={actionHandler}
										style={{
											background: 'linear-gradient(45deg, #7c4dff 30%, #b388ff  90%)',
											color: "white",
											height: "40px",
											width: "100%",
											minWidth: "70px"
										}}
									// className={classes.root}
									>
										{
											workFlow === "EDIT" ? "Update" : "Add"
										}
									</Button>
								</Col>
							</Row>
						</CardBody>
					</Card>
				</Col>
				{
					loader?
					<Spinner className="mt-4" style={{ width: '3rem', height: '3rem', color:"whitesmoke" }}  />
					:
					<>
					<Table className="align-items-center table-condensed table-striped mt-6 w3-animate-bottom"
					style={{
						marginLeft: "auto",
						marginRight: "auto",
						width: "550px",
						backgroundColor: "white",
					}}
					responsive>
					<thead className="thead-light"
					>
						{
							generateThs()
						}
					</thead>
					<tbody>
						{
							generateTds()
						}
					</tbody>
				</Table>
				<Row className="pagination mt-2 mb-5 w3-animate-bottom  d-flex justify-content-center">
				<Pagination
				nextPageStatus={nextPageStatus} 
				length={tableData && tableData.length}
				nextPage={newPage} 
				name="masterPageNo" getAllRecords={getAllRecords}
				 previousPage={oldPage} 
				 pageNo={pageNo} />
				</Row>
				</>
				}
				
			</div>
		</>
	);
};

const mapStateToProps = state =>{
	return{
		pageNo: state.pagination.masterPageNo,
		auth_token: state.login.data.auth_token
	}
}

const mapDispatchToProps = dispatch => {
	return {
		newPage: (item) => { dispatch(incrementPageNo(item)) },
		oldPage: (item) => { dispatch(decrementPageNo(item)) },
		logout: ()=> {dispatch(logout())}
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(Master);

