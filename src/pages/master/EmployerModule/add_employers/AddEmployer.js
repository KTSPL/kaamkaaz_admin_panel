/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable default-case */
import KK_Toast from 'components/Toast/KK_Toast'
import React, { useEffect, useState } from 'react'
import AddEmployerForm from 'views/AddEmployerForm/AddEmployerForm'
import {
	Button,
	FormGroup,
	Input,
	InputGroupAddon,
	InputGroupText,
	InputGroup,
	Row,
	Col,
	Card,
	CardHeader,
	CardBody
} from "reactstrap";
import Form from 'react-bootstrap/Form'
import apis from 'apis';
import { useHistory, useParams } from 'react-router';
import { connect } from 'react-redux';
import { setWorkFlow } from 'redux/employer/employer.action';
import { validation } from 'components/_helpers/validation'
import Map from 'components/MapModel/Map';
import Map2 from 'components/MapModel/Map2';
import { Multiselect } from 'multiselect-react-dropdown';


const AddEmployer = ({ address, setToastConfig, workFlow, employerId, setTab, setWorkFlow, setAddress, auth_token }) => {
	console.log(workFlow,employerId)
	const [errorMsgs, setErrorMsgs] = useState()
	const [enteredValue, setEnteredValue] = useState(null)
	const [errorCheckresult, setErrorCheckResult] = useState(null)
	console.log("Workflow check", workFlow === "EDIT")
	const [mapVisibility, setMapVisibility] = useState(false)
	const [flow,setFlow] = useState("ADD")
	const [prevData, setPrevData] = useState()
	let history = useHistory()
	const [formData, setFormData] = useState({
		firstname: null,
		lastname: null,
		companyname: null,
		contact1: null,
		contact2: null,
		industry_type_id: null,
		email: null,
		website: null,
		core_work: null,
		account_status:null

	});

	const [errStyle, setErrStyle] = useState({})

	const [fieldStyle, setFieldStyle] = useState(null)
	const [categoryList, setCategoryList] = useState([])
	

	// const onIndustrySelect = (e) =>{
	// 	let idArr=[]
	// 	e.map(i=>{
	// 		 idArr.push(i.id)
	// 	})
		
	// 	setFormData({...formData,industry_type_id:idArr})
	// }
	// const onIndustryRemove = (e) =>{
	// 	let idArr=[]
	// 	e.map(i=>{
	// 		 idArr.push(i.id)
	// 	})
	// 	setFormData({...formData,industry_type_id:idArr})
	// }

	useEffect(async () => {
		try {
			let res = await apis.masters.getAllRecords(auth_token, '/api/v1/master/jobCategory')
			
			if (res.success) {
				setCategoryList(res.data)
			}
			if (workFlow === "EDIT") {
				console.log(employerId)
				let response = await apis.masters.getSingleEmployer(auth_token,employerId)
				console.log("RESPONSE OF OLD DATA",response)
				let employerData= response.data.find(item=>item)
				setPrevData(employerData)
			}	
		}
		catch (err) {
			console.error(err.response)
		}
	}, [])

	useEffect(() => {
		console.log("PREV DATA IS THIS",prevData)
		if(prevData){
			setAddress([
				...prevData.locations
			])
		}
	}, [prevData])


	useEffect(()=>{
		console.log("CATEGORYLIST",categoryList)
	},[categoryList])

	useEffect(() => {
		if (workFlow === "EDIT" && prevData) {
			const {
				first_name,
				last_name,
				contact_number1,
				contact_number2,
				company_name,
				website,
				core_work,
				account_status,
				email,
				industry_type_id,
			} = prevData
			setFormData({
				industry_type_id: industry_type_id,
				companyname: company_name,
				contact1: contact_number1,
				contact2: contact_number2,
				website:website,
				core_work:core_work,
				account_status:account_status,
				email: email,
				firstname: first_name,
				lastname: last_name,
			})
		}
	}, [prevData])


	const handleSubmit = () => {
		scrollToTop()

		let resfirstname = validation.checkForEmpty(formData.firstname, "firstname")
		let reslastname = validation.checkForEmpty(formData.lastname, "reslastname")
		let rescompanyname = validation.checkForEmpty(formData.companyname, "companyname")
		let resindustry_type_id = validation.checkForEmpty(formData.industry_type_id, "industry_type_id")
		let resemail = validation.checkForEmpty(formData.email, "email")
		let reswebsite = validation.checkForEmpty(formData.website,"website")
		let rescore_work = validation.checkForEmpty(formData.core_work,"core_work")
		let resaccount_status = validation.checkForEmpty(formData.account_status,"account_status")
		let rescontact1 = validation.checkForEmpty(formData.contact1,"contact number")

		setErrorMsgs({
			...errorMsgs,
			firstname: { ...resfirstname },
			lastname: { ...reslastname },
			email: { ...resemail },
			companyname: { ...rescompanyname },
			industry_type_id: { ...resindustry_type_id },
			website:{...reswebsite},
			rescore_work:{...rescore_work},
			resaccount_status:{...resaccount_status},
			rescontact1:{...rescontact1}
		})

		setErrStyle({
			...errStyle,
			firstnamestyle: (resfirstname.validation === false) ? "error" : "success",
			lastnamestyle: (reslastname.validation === false) ? "error" : "success",
			emailstyle: (resemail.validation === false) ? "error" : "success",
			companynamestyle: (rescompanyname.validation === false) ? "error" : "success",
			industry_type_idstyle: (resindustry_type_id.validation === false) ? "error" : "success",
			contact1style:(rescontact1.validation===false)?"error":"success",
			websitestyle:(reswebsite.validation===false)? "error" : "sucess",
			core_workstyle:(rescore_work.validation===false)?"error":"success",
			account_statusstyle:(resaccount_status.validation===false)?"error":"success"
		})

		const { firstname, lastname, companyname, contact1, contact2, email, industry_type_id,core_work,account_status,website} = formData
		if ((!!firstname && lastname && companyname && email && industry_type_id && website)) {

			
			let body = {
				first_name: firstname,
				last_name: lastname,
				company_name: companyname,
				email: email,
				contact_number1: contact1,
				industry_type_id: industry_type_id,
				locations: address,
				account_status: parseInt(account_status),
				core_work:core_work,
				website:website
			}
			if (contact2) body.contact_number2 = contact2
			console.log("BODY BEFORE SUBMIT",body)
			if (workFlow === "EDIT") {
				body.employer_id= employerId
				UpdateEmployer(body)
			} else {
				createEmployer(body)
			}

			// clearHandler()
		}

	}

	const scrollToTop = () => {
		window.scrollTo(0, 0);
	}


	const createEmployer = async (body) => {
		console.log("Body",body)
		try {
			console.log(auth_token)
			let res = await apis.masters.createRecordRequest(auth_token, '/api/v1/master/employer', body)
			console.log(res.data)
			if(res.data.code==="ER_DUP_ENTRY"){
				setToastConfig({
					toastStatus: true,
					message: "Employer Already Exists",
					severity: "error"
				})
				return
			}
			if (res.success) {
				scrollToTop()
				setToastConfig({
					toastStatus: true,
					message: res.message,
					severity: "success"
				})
				setTab("LIST");
				setAddress([])
				return setWorkFlow("ADD")
			} else {
				scrollToTop()
				setToastConfig({
					toastStatus: true,
					message: res.message,
					severity: "error"
				})
			}
		}
		catch (err) {
			scrollToTop()
			setToastConfig({
				toastStatus: true,
				message: err.message,
				severity: "error"
			})
			console.error(err.response)
		}
	}

	const UpdateEmployer = async (body) => {
		address[address.length-1].employer_id= employerId;
		console.log("UPDATEBody----------------------------++++++++++++++++", body)
		try {
			let res = await apis.masters.updateRecordRequest(auth_token, "/api/v1/master/employer", employerId, body)
			if (res.success) {
				scrollToTop()
				setToastConfig({
					toastStatus: true,
					message: res.message,
					severity: "success"
				})
				setTab("LIST");
				return setWorkFlow("ADD")
			} else {
				scrollToTop()
				setToastConfig({
					toastStatus: true,
					message: res.message,
					severity: "error"
				})
			}
		}
		catch (err) {
			scrollToTop()
			setToastConfig({
				toastStatus: true,
				message: err.message,
				severity: "error"
			})
			console.error(err.response)
		}
	}

	const onChangeHandler = (e) => {

		switch (e.target.name) {
			case "firstname": {
				setErrStyle({
					...errStyle,
					[e.target.name + "style"]: null
				})
				setErrorMsgs({
					...errorMsgs,
					[e.target.name]: null
				})
				let res = validation.onlyLettersCheck(e.target.value, e.target.name)
			
				console.log("Response", res)
				if (res.validation === true) {
					setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "success"
					})
					setErrorMsgs({
						...errorMsgs,
						[e.target.name]: {
							...res
						}
					})
					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				} else {
					setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "error"
					})
					return setErrorMsgs({
						...errorMsgs,
						[e.target.name]: {
							...res
						}
					})
				}
			}
			case "lastname": {
				setErrStyle({
					...errStyle,
					[e.target.name + "style"]: null
				})
				setErrorMsgs({
					...errorMsgs,
					[e.target.name]: null
				})
				let res = validation.onlyLettersCheck(e.target.value, e.target.name)
				
				console.log("Response", res)
				if (res.validation === true) {
					setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "success"
					})

					setErrorMsgs({
						...errorMsgs,
						[e.target.name]: {
							...res
						}
					})
					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				} else {
					setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "error"
					})
					return setErrorMsgs({
						...errorMsgs,
						[e.target.name]: {
							...res
						}
					})
				}

			}

			case "companyname": {
			
				setErrStyle({
					...errStyle,
					[e.target.name + "style"]: null
				})
				setErrorMsgs({
					...errorMsgs,
					[e.target.name]: null
				})
					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				} 

			case "contact1": {
				setErrStyle({
					...errStyle,
					[e.target.name + "style"]: null
				})
				setErrorMsgs({
					...errorMsgs,
					[e.target.name]: null
				})
				let res = validation.onlyNumberCheck(e.target.value)
				console.log("Number Res", res)
				if (res.validation === true) {
					setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "success"
					})

					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				} else {
					setErrorMsgs({
						...errorMsgs,
						[e.target.name]: { ...res }
					})
					setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "error"
					})
					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				}
			}
			case "contact2": {
				setErrStyle({
					...errStyle,
					[e.target.name + "style"]: null
				})
				setErrorMsgs({
					...errorMsgs,
					[e.target.name]: null
				})
				let res = validation.onlyNumberCheck(e.target.value)
				console.log("Number Res", res)
				if (res.validation === true) {
					setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "success"
					})

					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				} else {
					setErrorMsgs({
						...errorMsgs,
						[e.target.name]: { ...res }
					})
					setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "error"
					})
					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				}
			}
			case "email": {
				setErrStyle({
					...errStyle,
					[e.target.name + "style"]: null
				})
				setErrorMsgs({
					...errorMsgs,
					[e.target.name]: null
				})
				let res = validation.emailCheck(e.target.value)
				console.log("Email Res", res)
				if (res.validation === true) {
					setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "success"
					})
					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				} else {
					
					setErrorMsgs({
						...errorMsgs,
						[e.target.name]: {
							...res
						}
					})
					 setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "error"
					})
					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
					
				}
			}
			case "industry_type_id": {
				setErrStyle({
					...errStyle,
					[e.target.name + "style"]: null
				})
				setErrorMsgs({
					...errorMsgs,
					[e.target.name]: null
				})
				return setFormData({
					...formData,
					[e.target.name]: e.target.value
				});
			}
			case "website": {
				setErrStyle({
					...errStyle,
					[e.target.name + "style"]: null
				})
				setErrorMsgs({
					...errorMsgs,
					[e.target.name]: null
				})
				return setFormData({
					...formData,
					[e.target.name]: e.target.value
				});
			}
			case "core_work": {
				setErrStyle({
					...errStyle,
					[e.target.name + "style"]: null
				})
				setErrorMsgs({
					...errorMsgs,
					[e.target.name]: null
				})
				return setFormData({
					...formData,
					[e.target.name]: e.target.value
				});
			}
			case "account_status": {
				setErrStyle({
					...errStyle,
					[e.target.name + "style"]: null
				})
				setErrorMsgs({
					...errorMsgs,
					[e.target.name]: null
				})
				return setFormData({
					...formData,
					[e.target.name]: e.target.value
				});
			}
			// case "address1": {
			// 	setErrStyle({
			// 		...errStyle,
			// 		[e.target.name + "style"]: null
			// 	})
			// 	setErrorMsgs({
			// 		...errorMsgs,
			// 		[e.target.name]: null
			// 	})
			// 	return setFormData({
			// 		...formData,
			// 		[e.target.name]: e.target.value
			// 	});
			// }
			// case "address2": {
			// 	return setFormData({
			// 		...formData,
			// 		[e.target.name]: e.target.value
			// 	});
			// }
			// case "city": {
			// 	setErrStyle({
			// 		...errStyle,
			// 		[e.target.name + "style"]: null
			// 	})
			// 	setErrorMsgs({
			// 		...errorMsgs,
			// 		[e.target.name]: null
			// 	})
			// 	return setFormData({
			// 		...formData,
			// 		[e.target.name]: e.target.value
			// 	});
			// }
			// case "state": {
			// 	setErrStyle({
			// 		...errStyle,
			// 		[e.target.name + "style"]: null
			// 	})
			// 	setErrorMsgs({
			// 		...errorMsgs,
			// 		[e.target.name]: null
			// 	})
			// 	return setFormData({
			// 		...formData,
			// 		[e.target.name]: e.target.value
			// 	});
			// }
			// case "pincode": {
			// 	setErrStyle({
			// 		...errStyle,
			// 		[e.target.name + "style"]: null
			// 	})
			// 	setErrorMsgs({
			// 		...errorMsgs,
			// 		[e.target.name]: null
			// 	})
			// 	return setFormData({
			// 		...formData,
			// 		[e.target.name]: e.target.value
			// 	});
			// }
		}

	}


	useEffect(()=>{
		console.log("FORMDATA",formData)
	},[formData])

	const clearHandler = () => {
		setErrStyle({})
		setErrorMsgs({})
		setFormData({
			firstname: null,
			lastname: null,
			companyname: null,
			contact1: null,
			contact2: null,
			address1: null,
			address2: null,
			email: null,
			city: null,
			state: null,
			pincode: null
		})
		document.getElementById("myform").reset();
	}

	const removeAddressTag = (index) => {
		let newAddress = []
		newAddress = address.filter((item, idx) => {
			return idx !== index
		})
		console.log("New Address", newAddress)
		setAddress(newAddress)
	}

	useEffect(() => {

	}, [errorMsgs])

	// const errormsggenerator=(name) =>{
	// 	return (
	// 		!(errorMsgs && errorMsgs[name] && errorMsgs[name].validation) && <small className="errText">{errorMsgs && errorMsgs[name] && errorMsgs[name].errMsg}</small>
	// 	)
	// }

	const addressHandler = () => {
		setMapVisibility(true)
	}

	useEffect(() => {
		console.log(formData)
	}, [formData])

	useEffect(() => {


	}, [errorMsgs])

	useEffect(() => {
		console.log("errStyle", errStyle)
	}, [errStyle])

	const [fadeout, setFadeOut] = useState({
		id: null,
		status: false
	})
	useEffect(() => {
		console.log("FADE OUT", fadeout)
	}, [fadeout])
	
	const [selectedLocation,setSelectedLocation]= useState({
		address_line1: null,
        address_line2: null,
        landmark: null,
        city: null,
        state: null,
        country: null,
        zip: null,
        geopoint_lat: null,
        geopoint_long: null,
	})

	const [selectedIdx,setSelectedIdx] = useState(null)
	
	return (
		<>
			{
				mapVisibility&&<Map workFlow={flow} address={address} selectedLocation={selectedLocation} selectedIdx={selectedIdx} setAddress={setAddress} setMapVisibility={setMapVisibility} isOpen={mapVisibility} setWorkFlow={setFlow} />
			}
			
			<Card className="addEmployer w3-animate-bottom" style={{
				width: "100%"
			}}>
				{/* <CardHeader>
						
					</CardHeader> */}
				<CardBody >
					<Form id="myform" autoComplete="none">
						<Row>
							<Col className="custombrdrright" md="12" lg="6" xl="6  p-4">
								<Form.Group controlId="name">
									<Row>
										<Col >
											<Form.Label className="mandatory">First    </Form.Label>
											<Form.Control autoComplete="none" className={errStyle.firstnamestyle} value={formData.firstname} onChange={onChangeHandler} name="firstname" type="text" placeholder="Enter First Name" />
											{
												!(errorMsgs && errorMsgs.firstname && errorMsgs.firstname.validation) && <small className="errText">{errorMsgs && errorMsgs.firstname && errorMsgs.firstname.errMsg}</small>
											}

										</Col>
										<Col>
											<Form.Label className="mandatory">Last Name</Form.Label>
											<Form.Control autoComplete="none" className={errStyle.lastnamestyle} type="text" value={formData.lastname} onChange={onChangeHandler} name="lastname" placeholder="Enter Last Name" />
											{
												!(errorMsgs && errorMsgs.lastname && errorMsgs.lastname.validation) && <small className="errText">{errorMsgs && errorMsgs.lastname && errorMsgs.lastname.errMsg}</small>
											}
										</Col>
									</Row>
								</Form.Group>
								<Form.Group controlId="companyName">
									<Form.Label className="mandatory">Company Name</Form.Label>
									<Form.Control name="companyname" autoComplete="none" className={errStyle.companynamestyle} type="text" value={formData.companyname} onChange={onChangeHandler} placeholder="Enter Company Name" />
									{
										!(errorMsgs && errorMsgs.companyname && errorMsgs.companyname.validation) && <small className="errText">{errorMsgs && errorMsgs.companyname && errorMsgs.companyname.errMsg}</small>
									}
								</Form.Group>
								<Form.Group controlId="addprimarycontactno">
									<Form.Label className='mandatory'>Contact Number 1</Form.Label>
									<Form.Control autoComplete="none" className={errStyle.contact1style} name="contact1" value={formData.contact1} onChange={onChangeHandler} type="text" placeholder="Enter First Contact" />
									{
										!(errorMsgs && errorMsgs.contact1 && errorMsgs.contact1.validation) && <small className="errText">{errorMsgs && errorMsgs.contact1 && errorMsgs.contact1.errMsg}</small>
									}
								</Form.Group>
								<Form.Group controlId="addsecondarycontactno">
									<Form.Label className="mandatory">Contact Number 2</Form.Label>
									<Form.Control autoComplete="none" className={errStyle.contact2style} name="contact2" value={formData.contact2} onChange={onChangeHandler} type="text" placeholder="Enter Second Contact" />
									{
										!(errorMsgs && errorMsgs.contact2 && errorMsgs.contact2.validation) && <small className="errText">{errorMsgs && errorMsgs.contact2 && errorMsgs.contact2.errMsg}</small>
									}
								</Form.Group>
								<Form.Group controlId="addemail">
									<Form.Label className="mandatory">E-mail</Form.Label>
									<Form.Control autoComplete="none" className={errStyle.emailstyle} name="email" value={formData.email} onChange={onChangeHandler} type="text" placeholder="Enter Email" />
									{
										!(errorMsgs && errorMsgs.email && errorMsgs.email.validation) && <small className="errText">{errorMsgs && errorMsgs.email && errorMsgs.email.errMsg}</small>
									}
								</Form.Group>
								<Form.Group controlId="industry_type_id">
									<Form.Label className="mandatory">Industry Type</Form.Label>
									{/* <Multiselect
										options={categoryList}
										displayValue="name"
										onSelect={onIndustrySelect}
										onRemove={onIndustryRemove}

									/> */}
									<Form.Control autoComplete="none" className={errStyle.industry_type_idstyle} name="industry_type_id" value={formData.industry_type_id} onChange={onChangeHandler} as="select">
										<option >Select Industry Type</option>
										{
											(categoryList!==null) && categoryList.map((category) => {
												return <option key={category.id} value={category.id}>{category.name}</option>
											})
										}
									</Form.Control>
									{
										!(errorMsgs && errorMsgs.industry_type_id && errorMsgs.industry_type_id.validation) && <small className="errText">{errorMsgs && errorMsgs.industry_type_id && errorMsgs.industry_type_id.errMsg}</small>
									}
								</Form.Group>
							</Col>
							<Col xl="6 p-4" lg="6" md="12">
							<Form.Group controlId="website">
									<Form.Label className="mandatory">Company Website</Form.Label>
									<Form.Control autoComplete="none" className={errStyle.websitestyle} name="website" value={formData.website} onChange={onChangeHandler} type="text" placeholder="Enter Website Url" />
									{
										!(errorMsgs && errorMsgs.website && errorMsgs.website.validation) && <small className="errText">{errorMsgs && errorMsgs.website && errorMsgs.website.errMsg}</small>
									}
								</Form.Group>
							<Form.Group controlId="corework">
									<Form.Label className="mandatory">Core Work Description</Form.Label>
									<Form.Control autoComplete="none" className={errStyle.core_workstyle} name="core_work" value={formData.core_work} onChange={onChangeHandler} type="text" placeholder="Enter work description" />
									{
										!(errorMsgs && errorMsgs.core_work && errorMsgs.core_work.validation) && <small className="errText">{errorMsgs && errorMsgs.core_work && errorMsgs.core_work.errMsg}</small>
									}
								</Form.Group>
								<Form.Group controlId="account_status">
									<Form.Label className="mandatory">Account Status</Form.Label>
									<Form.Control autoComplete="none" className={errStyle.account_statusstyle} name="account_status" value={formData.account_status} onChange={onChangeHandler} as="select">
										<option value={null}>Select Account Status</option>
										{/* {
											categoryList.map((category) => {
												return <option key={category.id} value={category.id}>{category.name}</option>
											})
										} */}
										<option value={1}>Active</option>
										<option value={0}>Suspend</option>
									</Form.Control>
									{
										!(errorMsgs && errorMsgs.account_status && errorMsgs.account_status.validation) && <small className="errText">{errorMsgs && errorMsgs.account_status && errorMsgs.account_status.errMsg}</small>
									}
								</Form.Group>
								<Form.Group
								//  style={{display:"flex", justifyContent:"flex-end"}}
								>
									<Button style={{ background: "white" }} className="text-primary" onClick={()=>{setFlow("ADD"); addressHandler();}}>
									<i class="fas fa-map-marked-alt mr-2"></i> ADD COMPANY LOCATION
							</Button>
									{/* <font style={{background:"white"}} className="text-primary" onClick={addressHandler}>
							<i class="fas fa-plus"></i> ADD ADDRESS
							</font> */}
								</Form.Group>
								
								{
									address && address.map((item, idx) => {
										const { address_line1, address_line2, city, state, zip, country,landmark } = item
										return (
											<div key={idx} className={fadeout.id === idx && fadeout.status ? "fadeOut w3-animate-bottom" : "w3-animate-bottom"}>
												<FormGroup className="addressTag">
													<Form.Label style={{ width: "100%", display: "flex", justifyContent: "space-between" }}><strong><i class="fas fa-map-marker-alt mr-2"></i> Location {idx + 1}</strong>
														{/* <font
															onClick={() => {
																setFadeOut({
																	id: idx,
																	status: true
																})
																setTimeout(() => {
																	removeAddressTag(idx)
																	setFadeOut({
																		id: idx,
																		status: false
																	})
																}, 500)
															}}
														>&#10005;</font> */}
														<i class="fas fa-pencil-alt pointer"
														onClick={async ()=>{
															setFlow("EDIT")
															setSelectedLocation(item)
															setSelectedIdx(idx)
															setMapVisibility(true)
															}}
														></i>
													</Form.Label>
													<div>
														{address_line1 + ","+ landmark + "," + address_line2 + ","+ city + "," + state + "," + zip}
													</div>
												</FormGroup>
											</div>
										)
									})
								}

								<Form.Group className="mt-7 pt-1">
									<Row >
										<Col xl="4  ml-auto" lg="6 d-flex justify-content-center" md="6" sm="6" xs="12 mt-1">
											<Button
												// onClick={handleNext}
												className="btnres ripple"
												style={{
													background: 'linear-gradient(45deg, #7c4dff 30%, #b388ff  90%)',
													color: "white",
												}}
												onClick={handleSubmit}
											>
												{
													workFlow === 'EDIT' ? "Update" : "Submit"
												}
											</Button>
										</Col>

										<Col xl="4 " lg="6 d-flex justify-content-center" md="6" sm="6" xs="12 mt-1">
											<Button
												onClick={() => {
													setTab("LIST")
													setWorkFlow("ADD")
													setAddress([])
												}}
												// disabled
												// style={{
												// 	background: "#ffebee",
												// 	color: "#f44336",
												// 	border: "1px solid #ef9a9a"
												// }}
												className="btnres ripple bg-red text-white">
												Cancel
                          			</Button>
										</Col>
									</Row>
								</Form.Group>
							</Col>
						</Row>
					</Form>
				</CardBody>
			</Card>
		</>
	)
}

const mapStateToProps = state => {
	return {
		auth_token: state.login.data.auth_token
	}
}

export default connect(mapStateToProps)(AddEmployer)

