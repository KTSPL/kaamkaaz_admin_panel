/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import EmployerListTable from 'views/EmployerListTable/EmployerListTable';
import Pagination from '../../../../components/pagination/Pagination'
import { useHistory } from 'react-router-dom';
import {
	Card,
	CardBody,
	CardHeader,
	Col, Row,
	Badge,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	Media,
	Progress,
	Table,
	Button,
	UncontrolledTooltip,
	Spinner
} from 'reactstrap';
import apis from 'apis';
import AddEmployerForm from 'views/AddEmployerForm/AddEmployerForm';
import KK_Toast from 'components/Toast/KK_Toast';
import { connect } from 'react-redux';
import { incrementPageNo } from 'redux/pagination/pagination.action';
import { decrementPageNo } from 'redux/pagination/pagination.action';
import EmptyTablePlaceHolder from 'components/EmptyTableReplacement.js/EmptyTablePlaceHolder';
import { logout } from 'redux/login/login.action';


const EmployerListPage = ({ setEmployerId, setToastConfig, setTab, setWorkFlow, pageNo, nextPage, previousPage, auth_token,logout }) => {
	let history = useHistory()
	const [tableData, setTableData] = useState([])
	const [deleteId, setDeleteId] = useState()
	const [nextPageStatus, setNextPageStatus] = useState(false)
	const [status, setStatus] = useState(null)
	const [loader, setLoader] = useState(false)

	useEffect(async () => {
		setLoader(true)
		console.log(pageNo)
		try {
			let res = await apis.masters.getAllEmployer(auth_token, `/api/V1/master/employer`)
		
			if (res.data.length > 0) {
				setLoader(false)
				setNextPageStatus(true)
			} else {
				setLoader(false)
				setNextPageStatus(false)
			}
			console.log(res)
		}
		catch (err) {
			setLoader(false)
			if(err.response&&err.response.status===401){
				setToastConfig({
					toastStatus:true,
					message: 'Session Expired!',
					severity:'error'
				})
				setTimeout(()=>{
					logout()
				},2000)
			}
			
		}
		getAllRecords()
	}, [])

	useEffect(() => {
		getAllRecords()
	}, [pageNo])


	useEffect(() => {
		console.log(tableData)
	}, [tableData])

	const generateThs = () => {
		let res = tableData.filter((data, idx) => idx < 1).map(data => {
			let values = Object.getOwnPropertyNames(data)
			return (
				values.map(item => {
					return (
						<th className='text-white' scope="col">{item}</th>
					)
				})
			)
		})
		return res
	}


	const generateTds = () => {
		//    let tdList=[]
		//    let res= []
		let res = tableData.map(data => {
			let values = Object.values(data)
			return (
				<tr>{
					values.map(item => {
						return (
							<>
								<td scope="col">{item}</td>
							</>
						)
					})}
					<td className="text-right">
						<UncontrolledDropdown>
							<DropdownToggle
								className="btn-icon-only text-light"
								// href="#pablo"
								role="button"
								size="sm"
								color=""
								onClick={e => e.preventDefault()}
							>
								<i className="fas fa-ellipsis-v" />
							</DropdownToggle>
							<DropdownMenu className="dropdown-menu-arrow" right>
								<DropdownItem
									name="EDIT"
									onClick={(e) => {
										setEmployerId(data.id)
										setTab("ADD")
										setWorkFlow("EDIT")
									}}
								>
									Edit
								</DropdownItem>
								<DropdownItem
									name="updateStatus"
									onClick={() => { changeStatus(data.id, data.is_active) }}
								>
									{data.is_active == 0 ? "Activate" : "Deactivate"}
								</DropdownItem>
							</DropdownMenu>
						</UncontrolledDropdown>
					</td>
				</tr>

			)

		})
		return res
	}

	const changeStatus = async (deleteId, status) => {
		console.log(deleteId)
		let tempStatus = 0
		if (deleteId && status) {
			if (status == 0) {
				tempStatus = 1
			}
			try {
				let res = await apis.masters.updateStatus(auth_token, '/api/v1/master/employer', deleteId, tempStatus)
				if (res.success) {
					scrollToTop()
					setToastConfig({
						toastStatus: true,
						message: res.message,
						severity: "success"
					})
					return getAllRecords()
				} else {
					scrollToTop()
					return setToastConfig({
						toastStatus: true,
						message: res.message,
						severity: "error"
					})
				}
			}
			catch (err) {
				scrollToTop()
				setToastConfig({
					toastStatus: true,
					message: err.message,
					severity: "error"
				})
				console.error(err)
			}
		}
	}

	


	const scrollToTop = () => {
		window.scrollTo(0, 0);
	}


	const getAllRecords = async () => {
		setLoader(true)
		try {
			let res = await apis.masters.getAllEmployer(auth_token, `/api/v1/master/employerpage/${pageNo === 0 ? "0" : pageNo}`)
			if (res.success) {
				setTableData(res.data)
				setLoader(false)
			}
		}
		catch (err) {
			if(err.response&&err.response.status===401){
				setToastConfig({
					toastStatus:true,
					message: 'Session Expired!',
					severity:'error'
				})
				setTimeout(()=>{
					logout()
				},2000)
			}
			setLoader(false)
			console.error(err)
		}
	}



	return (
		<>
			{
				loader?
				<Spinner style={{ width: '3rem', height: '3rem', color:"whitesmoke" }}  />
						:
				(tableData && tableData.length > 0)?		
						<>
							<Table className="align-items-center table-condensed table-striped  w3-animate-bottom"
								style={{
									marginLeft: "auto",
									marginRight: "auto",
									// width: "550px",
									backgroundColor: "white",
								}}
								responsive>
								<thead className="thead-dark"
								>
									{
										generateThs()
									}
									<th className='text-white' scope="col">Actions</th>
								</thead>
								<tbody>
									{
										generateTds()
									}
								</tbody>
							</Table>
							<Pagination
								nextPageStatus={nextPageStatus}
								length={tableData && tableData.length}
								nextPage={nextPage} name="empPageNo" getAllRecords={getAllRecords} previousPage={previousPage} pageNo={pageNo} />
						</>

					:
					<EmptyTablePlaceHolder>
						No Employers Found
					</EmptyTablePlaceHolder>
			}

		</>
	);
};

const mapStateToProps = state => {
	return {
		pageNo: state.pagination.empPageNo,
		auth_token: state.login.data.auth_token
	}
}

const mapDispatchToProps = dispatch => {
	return {
		logout: ()=> {dispatch(logout())},
		nextPage: (item) => { dispatch(incrementPageNo(item)) },
		previousPage: (item) => { dispatch(decrementPageNo(item)) }
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(EmployerListPage);