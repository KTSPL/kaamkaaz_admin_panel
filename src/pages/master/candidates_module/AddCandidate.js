import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import { Multiselect } from "multiselect-react-dropdown";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Badge,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Progress,
  Table,
  Button,
  CustomInput,
  Label,
  FormGroup,
  UncontrolledTooltip,
} from "reactstrap";
import { useSelector } from "react-redux";
import apis from "apis";
import { useHistory, useParams } from "react-router";
import KK_Toast from "components/Toast/KK_Toast";
import CustomFileInput from "components/Custom_file_input/CustomFileInput";
import SelectComponent from "components/customSelect/Select.Component";
import { validation } from "components/_helpers/validation";


const AddCandidate = ({setWorkFlow,setTab,workFlow,setToastConfig,toastStatus, candidateData}) => {
  const [resume, setResume] = useState(null);
  const auth_token = useSelector((state) => state.login.data.auth_token);
  const [skillList, setSkillList] = useState([]);
  const params = useParams();
  const history = useHistory();
  const pathName = history.location.pathname;
  const [inputClass, setInputClass] = useState("normal");
  const [selectedContent, setSelectedContent] = useState();
  const [tableData, setTableData] = useState([]);
  const [enteredValue, setEnteredValue] = useState("");
  const [operableData, setOperableData] = useState("");
  const [industryList, setIndustryList] = useState("");
  const [experienceList, setExperienceList] = useState("");
  const [qualificationList, setqualificationList] = useState("");
  const [voterId,setVoterId] = useState(null)
  const [selectedCv, setSelectedCv] = useState();
	const [isCvPicked, setIsCvPicked] = useState(false);
  const [selectedIdentity, setSelectedIdentity] = useState();
	const [isIdentityPicked, setIsIdentityPicked] = useState(false);
  // const [workFlow, setWorkFlow] = useState("ADD");
  const [enterMax, setMax] = useState(null);
  const [errStyle, setErrStyle] = useState({});
  const [formData, setFormData] = useState({
    full_name:null,
    mobile_no: null,
    email_id:null,
    job_category: null,
    skills: null,
    experience: null,
    education_qualification:null,
    upload_resume:null,
    upload_aadhaar:null
  });
  const [errorMsgs, setErrorMsgs] = useState();
  const [prevData,setPrevData]= useState({})


  const cvChangeHandler = (event) => {
		setSelectedCv(event.target.files[0]);
		setIsCvPicked(true);
	};
  const identityChangeHandler = (event) => {
		setSelectedIdentity(event.target.files[0]);
		setIsIdentityPicked(true);
	};

 
  useEffect(()=>{
    if(candidateData&&candidateData.user_id){
      setPrevData({
        ...candidateData
      })
    }
  },[])

  useEffect(()=>{
    if(workFlow==="EDIT"){
      setFormData({
        ...prevData
      })
    }
  },[prevData])

  const scrollToTop = () => {
    window.scrollTo(0, 0);
  };

  useEffect( () => {
    console.log(selectedCv)
    async function uploadCv(){
      var bodyFormData = new FormData();
      bodyFormData.append('cv', selectedCv); 
      try{
        let res = await apis.candidates.documentUpload("/cv_upload",bodyFormData)
        console.log(res)
        setFormData({
          ...formData,
          upload_resume:res.resultData.filePath
      })
      }catch(err){
        console.error(err.response)
      }
    }
    uploadCv()
  
    
  }, [selectedCv]);

  useEffect(()=>{
    console.log(selectedIdentity)
    async function uploadIdentity(){
      var bodyFormData = new FormData();
      bodyFormData.append('doc', selectedIdentity); 
      try{
        let res = await apis.candidates.documentUpload("/doc_upload",bodyFormData)
        console.log(res)
        setFormData({
          ...formData,
          upload_aadhaar: res.resultData.filePath
        })
      }catch(err){
        console.error(err.response)
      }
    }
    uploadIdentity()
    
  },[selectedIdentity])

useEffect(()=>{
console.log("FormData", formData)
},[formData])

useEffect(()=>{
  console.log("voterId",voterId)
},[voterId])

  //Component Did mount
  useEffect(async () => {
    //api call to populate the table as per the req in content object
    getAllRecords("/api/v1/master/jobCandidate");
    try {
      let skills = await apis.masters.getAllRecords(
        auth_token,
        "/api/v1/master/jobSkill"
      );
      let industryList = await apis.masters.getAllRecords(
        auth_token,
        "/api/v1/master/jobCategory"
      );
      let qualificationList = await apis.masters.getAllRecords(
        auth_token,
        "/api/v1/master/educationalQualification/"
      );
      let experienceList = await apis.masters.getAllRecords(
        auth_token,
        "/api/v1/master/jobExperience"
      );
      if (skills.success === true) {
        setSkillList(skills.data);
      }
      if (industryList.success === true) {
        setIndustryList(industryList.data);
      }
      if (qualificationList.success === true) {
        setqualificationList(qualificationList.data);
      }
      if (experienceList.success === true) {
        setExperienceList(experienceList.data);
      }
    } catch (error) {
      console.log(error);
    }
  }, []);

  const onSelect=(e)=>{
    setErrorMsgs({
      ...errorMsgs,
      skills:null
    })
    console.log("E FROM MULTI SELECT",e)
    let ls = e.map(i=>i.name).toString()

    console.log(ls)
    setFormData({
        ...formData,
        skills:ls
    })
  }

  const onRemove=(e)=>{

  }

  const onChangeHandler = (e) => {
   
      setErrStyle({
          ...errStyle,
          [e.target.name+ "style"]: null
      })
      setErrorMsgs({
          ...errorMsgs,
          [e.target.name]: null
      })
      

      if(e.target.name==="mobile_no"){
        let res = validation.onlyNumberCheck(e.target.value) 
        if (res.validation === true) {
					setErrStyle({
						...errStyle,
						[e.target.name + "style"]: "success"
					})

					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				} else {
					setErrorMsgs({
						...errorMsgs,
						[e.target.name]: { ...res }
					})
					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				}
      }else if(e.target.name==="email_id"){
				let res = validation.emailCheck(e.target.value)
				console.log("Email Res", res)
				if (res.validation === true) {
					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
				} else {
					setErrorMsgs({
						...errorMsgs,
						[e.target.name]: {
							...res
						}
					})
					return setFormData({
						...formData,
						[e.target.name]: e.target.value
					});
					
				}
      }
      else{

      setFormData({...formData,
          [e.target.name]: e.target.value
      })
    }
  };

  
  useEffect(() => {
    console.log(enteredValue);
  }, [enteredValue]);

  useEffect(() => {
    setEnteredValue(operableData.name);
    console.log("workFlow", workFlow);
  }, [workFlow]);

  const getAllRecords = async (path) => {
    try {
      let res = await apis.masters.getAllRecords(path);
      console.log("Real Response", res);
      if (res.success === true) {
        setTableData(res.data);
      }
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {}, [qualificationList, experienceList, industryList]);

  useEffect(()=>{
    console.log(errorMsgs)
  },[errorMsgs])

  const actionHandler = async (e) => {
    let res={};

    let formDataKeys = Object.keys(formData)
    formDataKeys.map(i=>{
       res["res"+i]=null
    })

    formDataKeys.map(i=>{
        res["res"+i]= validation.checkForEmpty(formData[i],i)
    })

    let errmsg={}
   formDataKeys.map(i=>{
     
    errmsg[i]={...res["res"+i]}
       
   })
   if(voterId===null){
    errmsg.voterId ={
      validation: false,
      errMsg: "Identity Proof is required"
    }

   }

   if(skillList.length<1){
    errmsg.skills ={
      validation: false,
      errMsg: "Identity Proof is required"
    }
   }

   setErrorMsgs({
        ...errmsg
   })

  
  

   const {
    full_name,
    mobile_no,
    email_id,
    job_category,
    skills,
    experience,
    education_qualification,
    upload_resume,
    upload_aadhaar
   }= formData

   let body={
    full_name,
    mobile_no,
    email_id,
    job_category,
    skills,
    experience,
    education_qualification,
    upload_resume
   }
   console.log("body", body)
   
   if(!!(full_name && mobile_no && email_id && job_category && skills && experience && education_qualification && upload_aadhaar)){
       let body={
        full_name,
        mobile_no,
        email_id,
        job_category,
        skills,
        experience,
        education_qualification,
        upload_aadhaar
       }
       console.log("body", body)
       if(prevData.user_id) body.user_id=prevData.user_id
       if(upload_resume) body.upload_resume = upload_resume
       if(workFlow==="ADD"){
        try{
          let res = await apis.candidates.addCandidates(body)
        console.log(res)
        // if(res.resultCode===1){
          setToastConfig({
            toastStatus: true,
            message: res.resultMsg,
            severity: "success"
          })
          setTab("LIST")
        // }
        }catch(err){
          console.log(err.response)
          setToastConfig({
            toastStatus: true,
            message: err.message,
            severity: "error"
          })
        }
       }else if(workFlow==="EDIT"){
         console.log("EDIT WAS CALLED")
        try{
          let res = await apis.candidates.updateCandidate(body)
        console.log(res)
        // if(res.resultCode===1){
          setToastConfig({
            toastStatus: true,
            message: res.resultMsg,
            severity: "success"
          })
          setTab("LIST")
        // }
        }catch(err){
          console.error(err.response)
          setToastConfig({
            toastStatus: true,
            message: err.message,
            severity: "error"
          })
        }
      }

   }
  };



  return (
    <>
      <Card style={{ width: "100%" }}>
        <CardHeader className="pl-4 pt-2 pb-0">
          <Form.Label>Add Candidates</Form.Label>
        </CardHeader>
        <CardBody className="pt-3 pb-0">

          <Form id="myform" autoComplete="none">
            <Row className="p-0">
              {/* <Col xs="12 " sm={selectedContent && selectedContent.id === 2 ? "12 pl-3" : "9 pl-3"} md={selectedContent && selectedContent.id === 2 ? "12 pl-3" : "9 pl-3"} lg={selectedContent && selectedContent.id === 2 ? "12 pl-3" : "9 pl-3"}> */}
              {/* <Col xs="12"> */}
              <Col className="custombrdrright" md="12" lg="6" xl="6  p-4">
                <Form.Group controlId="full_name">
                  <Form.Label className="mandatory">Full Name</Form.Label>
                  <Form.Control
                    name="full_name"
                    autoComplete="none"
                    className={errStyle.full_namestyle}
                    type="text"
                    value={formData.full_name}
                    onChange={onChangeHandler}
                    placeholder="Enter Full Name"
                  />
                  {!(
                    errorMsgs &&
                    errorMsgs.full_name &&
                    errorMsgs.full_name.validation
                  ) && (
                    <small className="errText">
                      {errorMsgs &&
                        errorMsgs.full_name &&
                        errorMsgs.full_name.errMsg}
                    </small>
                  )}
                </Form.Group>
                <Form.Group controlId="email_id">
                  <Form.Label className="mandatory">Email Address</Form.Label>
                  <Form.Control
                    name="email_id"
                    autoComplete="none"
                    className={errStyle.email_idstyle}
                    type="text"
                    value={formData.email_id}
                    onChange={onChangeHandler}
                    placeholder="Enter Email"
                  />
                  {!(
                    errorMsgs &&
                    errorMsgs.email_id &&
                    errorMsgs.email_id.validation
                  ) && (
                    <small className="errText">
                      {errorMsgs &&
                        errorMsgs.email_id &&
                        errorMsgs.email_id.errMsg}
                    </small>
                  )}
                </Form.Group>
                <Form.Group controlId="mobile_no">
                  <Form.Label className="mandatory">Mobile Number</Form.Label>
                  <Form.Control
                    name="mobile_no"
                    autoComplete="none"
                    className={errStyle.mobile_nostyle}
                    type="text"
                    value={formData.mobile_no}
                    onChange={onChangeHandler}
                    placeholder="Enter mobile number"
                  />
                  {!(
                    errorMsgs &&
                    errorMsgs.mobile_no &&
                    errorMsgs.mobile_no.validation
                  ) && (
                    <small className="errText">
                      {errorMsgs &&
                        errorMsgs.mobile_no &&
                        errorMsgs.mobile_no.errMsg}
                    </small>
                  )}
                </Form.Group>
                <Form.Group controlId="job_category">
                  <Form.Label className="mandatory">Industry Type</Form.Label>
                  <SelectComponent value={formData.job_category} style={{ width: "100%" }} name="job_category" onChange={onChangeHandler}>
                    <option value={null} style={{ overflowX: "hidden" }}>
                      Industry
                    </option>
                    {industryList &&
                      industryList.map((item) => {
                        return <option value={item.name}>{item.name}</option>;
                      })}
                  </SelectComponent>
                  {!(
                    errorMsgs &&
                    errorMsgs.job_category &&
                    errorMsgs.job_category.validation
                  ) && (
                    <small className="errText">
                      {errorMsgs &&
                        errorMsgs.job_category &&
                        errorMsgs.job_category.errMsg}
                    </small>
                  )}
                </Form.Group>
              </Col>
              <Col className="custombrdrright" md="12" lg="6" xl="6  p-4">
                <Form.Group controlId="skills">
                  <Form.Label className="mandatory">Skills</Form.Label>
                  <Multiselect onSelect={onSelect} onRemove={onRemove} value={formData.skills} options={skillList} displayValue="name" />
                  {!(
                    errorMsgs &&
                    errorMsgs.skills &&
                    errorMsgs.skills.validation
                  ) && (
                    <small className="errText">
                      {errorMsgs &&
                        errorMsgs.skills &&
                        errorMsgs.skills.errMsg}
                    </small>
                  )}
                </Form.Group>
                <Form.Group controlId="experience">
                  <Form.Label className="mandatory">Experience</Form.Label>
                  <SelectComponent value={formData.experience} onChange={onChangeHandler}  name="experience" style={{ width: "100%" }}>
                   <option>Experience</option>
                    {experienceList &&
                      experienceList.map((item) => {
                        return (
                          <option value={(item.min_exp+"-"+item.max_exp)} >
                            {item.min_exp}-{item.max_exp}
                          </option>
                        );
                      })}
                  </SelectComponent>
                  {!(
                    errorMsgs &&
                    errorMsgs.experience &&
                    errorMsgs.experience.validation
                  ) && (
                    <small className="errText">
                      {errorMsgs &&
                        errorMsgs.experience &&
                        errorMsgs.experience.errMsg}
                    </small>
                  )}
                </Form.Group>
                <Form.Group controlId="education_qualification">
                  <Form.Label className="mandatory">
                    Educational Qualification
                  </Form.Label>
                  <SelectComponent value={formData.education_qualification} name="education_qualification" onChange={onChangeHandler} style={{ width: "100%" }}>
                    <option value={"default"} style={{ overflowX: "hidden" }}>
                      Qualification
                    </option>
                    {qualificationList &&
                      qualificationList.map((item) => {
                        return <option value={item.name}>{item.name}</option>;
                      })}
                  </SelectComponent>
                  {!(
                    errorMsgs &&
                    errorMsgs.education_qualification &&
                    errorMsgs.education_qualification.validation
                  ) && (
                    <small className="errText">
                      {errorMsgs &&
                        errorMsgs.education_qualification &&
                        errorMsgs.education_qualification.errMsg}
                    </small>
                  )}
                </Form.Group>
              
                <Form.Group controlId="upload_resume">
                  <Label  for="exampleCustomFileBrowser">Upload Resume</Label>
                  <CustomFileInput
                  id="resume"
                  onChange={cvChangeHandler} />
                  {!(
                    errorMsgs &&
                    errorMsgs.upload_resume &&
                    errorMsgs.upload_resume.validation
                  ) && (
                    <small className="errText">
                      {errorMsgs &&
                        errorMsgs.upload_resume &&
                        errorMsgs.upload_resume.errMsg}
                    </small>
                  )}
                  <h5>
                    {resume&&resume.split('\\').pop()}
                  </h5>
                </Form.Group>
                
                <Form.Group controlId="voter_id">
                  <Label className="mandatory" for="exampleCustomFileBrowser">
                    Upload Identity Proof
                  </Label>
                  <CustomFileInput
                  id="voterId"
                   onChange={identityChangeHandler}
                  
                  />
                 
                  {!(
                    errorMsgs &&
                    errorMsgs.voterId &&
                    errorMsgs.voterId.validation
                  ) && (
                    <small className="errText">
                      {errorMsgs &&
                        errorMsgs.voterId &&
                        errorMsgs.voterId.errMsg}
                    </small>
                  )}
                  <h5>
                    {voterId&&voterId.split('\\').pop()}
                  </h5>
                </Form.Group>
              </Col>
              {selectedContent && selectedContent.id === 2 && (
                <Col xs="12 " sm="12 pl-3" md="12 pl-3" lg={"12 pl-3"}>
                  <Form.Label>
                    {selectedContent && selectedContent.max}
                  </Form.Label>
                  <Form.Group controlId="language">
                    <Form.Control
                      type="text"
                      name="max"
                      onChange={onChangeHandler}
                      className={inputClass}
                      // value={enteredValue}
                      placeholder={selectedContent && selectedContent.max}
                    />
                    {inputClass === "error" && (
                      <small className="errText">
                        This Field Can't be Empty
                      </small>
                    )}
                  </Form.Group>
                </Col>
              )}
              <Row
                style={
                  selectedContent && selectedContent.id === 2
                    ? {
                        left: "50%",
                        transform: "translateX(-50%)",
                      }
                    : null
                }
                xs="12 d-flex justify-content-center pb-3 w-100"
                sm="3"
                md="3"
                lg={
                  selectedContent && selectedContent.id === 2
                    ? "3 pr-3"
                    : "3 pr-3"
                }
              >
                <Button
                  name="ADD"
                  className="ripple"
                  onClick={actionHandler}
                  style={{
                    background:
                      "linear-gradient(45deg, #7c4dff 30%, #b388ff  90%)",
                    color: "white",
                    height: "40px",
                    width: "100%",
                    minWidth: "70px",
                  }}
                  // className={classes.root}
                >
                  {workFlow === "EDIT" ? "Update" : "Add"}
                </Button>
              </Row>
            </Row>
          </Form>
        </CardBody>
      </Card>
    </>
  );
};

export default AddCandidate;
