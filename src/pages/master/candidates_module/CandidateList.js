/* eslint-disable react-hooks/exhaustive-deps */
import Pagination from '../../../components/pagination/Pagination'
import apis from 'apis';
import KK_Toast from 'components/Toast/KK_Toast';
import { scrollToTop } from 'components/_helpers/utils';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';

import {
	Card,
	CardBody,
	CardHeader,
	Col, Row,
	Badge,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	DropdownToggle,
	Media,
	Progress,
	Table,
	Button,
	UncontrolledTooltip,
	PaginationItem,
	PaginationLink,
	Spinner
} from 'reactstrap';
import { decrementPageNo } from 'redux/pagination/pagination.action';
import { incrementPageNo } from 'redux/pagination/pagination.action';

const CandidateList = ({ setCandidateData, setCandidateId, setToastConfig, setTab, setWorkFlow, pageNo, auth_token }) => {
	console.log("pageNo", pageNo)
	const [deleteId, setDeleteId] = useState()
	const [editId, setEditId] = useState()
	const [tableData, setTableData] = useState()
	const [datalength, setDatalength] = useState()
	const [page_no, setPageNo] = useState(0)
	const [nextPageStatus, setNextPageStatus] = useState(false)
	const [start, setStart] = useState(0);
	const [end, setEnd] = useState(9)
	const [nextStatus, setNextStatus] = useState(false)
	const [loader,setLoader] = useState(false)


	const previousPage = () => {
		setNextStatus(false)
		if (start > 0 && end > 9 && ((start - 10) >= 0) && ((end - 10) >= 9)) {
			setStart(i => i - 10)
			setEnd(i => i - 10)
		}
	}

	const nextPage = () => {
		if (end < tableData.length) {
			setStart(i => i + 10)
			setEnd(i => i + 10)
		} else {
			setNextStatus(true)
		}
	}

	useEffect(() => {
		console.log("start:", start, "  End:", end)
	}, [start, end])


	useEffect(() => {
		console.log("New TableDATA", tableData)
	}, [tableData])

	useEffect(async () => {
		setLoader(true)
		try {
			let res = await apis.candidates.getAllCandidates()
			if (res.resultCode === 1) {
				setLoader(false)
				setTableData(res.resultData)
			}
			console.log(res)
		} catch (err) {
			setLoader(false)
			console.log(err)
		}
		getAllRecords()
	}, [])

	useEffect(() => {
		getAllRecords()
	}, [pageNo])


	useEffect(async () => {
		console.log("deleteId", deleteId)
		if (deleteId) {
			try {
				let res = await apis.candidates.deleteCandidate(deleteId)
				console.log(res)
				if (res.resultCode === 1) {
					scrollToTop()
					setToastConfig({
						toastStatus: true,
						message: res.resultMsg,
						severity: "success"
					})
					return getAllRecords()
				} else {
					scrollToTop()
					return setToastConfig({
						toastStatus: true,
						message: res.message,
						severity: "error"
					})
				}
			}
			catch (err) {
				console.error(err.response)
				scrollToTop()
				setToastConfig({
					toastStatus: true,
					message: err.message,
					severity: "error"
				})
				console.error(err)
			}
		}
	}, [deleteId])


	useEffect(() => {
		if (editId) {
			history.push({
				pathname: "/admin/addJob/" + editId
			})
		}
	}, [editId])


	const generateThs = () => {
		let res = tableData && tableData.filter((data, idx) => idx < 1).map(data => {
			let values = Object.getOwnPropertyNames(data)
			return (
				values.map(item => {
					return (
						<th className='text-white' scope="col">{item}</th>
					)
				})
			)
		})
		return res
	}



	const generateTds = () => {
		//    let tdList=[]
		//    let res= []
		let res = tableData && tableData.filter((data, idx) => (idx >= start && idx <= end)).map(data => {
			let values = Object.values(data)
			return (
				<tr>{
					values.map(item => {
						return (
							<>
								<td scope="col">{item}</td>
							</>
						)
					})}
					<td className="text-right">
						<UncontrolledDropdown>
							<DropdownToggle
								className="btn-icon-only text-light"
								// href="#pablo"
								role="button"
								size="sm"
								color=""
								onClick={e => e.preventDefault()}
							>
								<i className="fas fa-ellipsis-v" />
							</DropdownToggle>
							<DropdownMenu className="dropdown-menu-arrow" right>
								<DropdownItem
									name="EDIT"
									onClick={(e) => {
										setCandidateData(data)
										setWorkFlow("EDIT")
										setTab("ADD")
									}}
								>
									Edit
								</DropdownItem>
								<DropdownItem
									name="DELETE"
									onClick={(e) => {
										setDeleteId(data.user_id);
									}}
								>
									Delete
								</DropdownItem>
							</DropdownMenu>
						</UncontrolledDropdown>
					</td>
				</tr>

			)

		})
		return res
	}

	const getAllRecords = async () => {
		setLoader(true)
		try {
			let res = await apis.candidates.getAllCandidates()
			console.log("res", res)
			if (res.resultCode === 1) {
				setLoader(false)
				setTableData(res.resultData)
			}
		}
		catch (err) {
			setLoader(false)
			console.error(err)
		}
	}

	let history = useHistory()
	return (
		<>
			{/* Table Code */}
			{
				loader ?
				<Spinner style={{ width: '3rem', height: '3rem', color:"whitesmoke" }}  />
				:
					<>
						<Table className="align-items-center table-condensed table-striped  w3-animate-bottom"
							style={{
								marginLeft: "auto",
								marginRight: "auto",
								backgroundColor: "white",
							}}
							responsive>
							<thead className="thead-dark"
							>
								{
									generateThs()
								}
								<th className='text-white' scope="col">Actions</th>
							</thead>
							<tbody>
								{
									generateTds()
								}
							</tbody>
						</Table>
						<Row className="pagination mt-2 mb-5 w3-animate-bottom">
							<Button className="mr-3 "
								disabled={start === 0 ? true : false}
								onClick={() => { previousPage() }}>&lt;</Button>
							<Button
								disabled={nextStatus}
								onClick={() => { nextPage() }}>&gt;</Button>
						</Row>
					</>
			}

		</>
	);
};


const mapStateToProps = state => {
	return {
		pageNo: state.pagination.jobPageNo,
		auth_token: state.login.data.auth_token
	}
}

const mapDispatchToProps = dispatch => {
	return {
		nextPage: (item) => { dispatch(incrementPageNo(item)) },
		previousPage: (item) => { dispatch(decrementPageNo(item)) }
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(CandidateList);