import React from 'react';
import KK_Toast from 'components/Toast/KK_Toast';
import { Col, Row } from 'reactstrap';

const ShowCase = () => {
    return (
        <div>
            <Row className="d-flex justify-content-center">
                <Col lg="5" style={{
                    position:"absolute"
                }} >
                    <KK_Toast />
                </Col>
            </Row>
        </div>
    );
};

export default ShowCase;