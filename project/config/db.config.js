'use strict';
const mysql = require('mysql');
const {createPool} = require("mysql");

const dbConn = createPool({
  connectionLimit: 100,
  host     : process.env.HOST,
  user     : process.env.USER_NAME,
  password : process.env.PASS,
  database : process.env.DB_NAME,
  multipleStatements: true
});

//server
/*const dbConn = mysql.createConnection({
  host     : 'localhost',
  user     : 'wwwktspl_kamkaj',
  password : 'bfi;itnJ=AT~',
  database : 'wwwktspl_dream_job_search'
});
*/

dbConn.getConnection(err => {
  if (err) throw err;
  console.log("Database Connected!");
});

module.exports = dbConn;