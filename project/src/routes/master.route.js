const express = require('express')
const app = express();
const router = express.Router()
const verifyToken = require('../validate/verifyToken');

/** Middleware to verify the jwt */
// router.use(verifyToken);

//Jot type routes
const jobTypeController = require('../controllers/jobType.controller');
router.get('/jobTypes', jobTypeController.findAll);
router.post('/jobTypes', jobTypeController.create);
router.get('/jobTypes/:id', jobTypeController.findById);
router.put('/jobTypes/:id', jobTypeController.update);
router.delete('/jobTypes/:id', jobTypeController.delete);

//job Experience routs
const jobExperienceController = require('../controllers/jobExperience.controller');
router.get('/jobExperience', jobExperienceController.findAll);
router.post('/jobExperience', jobExperienceController.create);
router.get('/jobExperience/:id', jobExperienceController.findById);
router.put('/jobExperience/:id', jobExperienceController.update);
router.delete('/jobExperience/:id', jobExperienceController.delete);


//job_category_master
const jobCategoryController = require('../controllers/jobCategory.controller');
router.get('/jobCategory', jobCategoryController.findAll);
router.post('/jobCategory', jobCategoryController.create);
router.get('/jobCategory/:id', jobCategoryController.findById);
router.put('/jobCategory/:id', jobCategoryController.update);
router.delete('/jobCategory/:id', jobCategoryController.delete);

//job_skills_master
const jobSkillController = require('../controllers/jobSkill.controller');
router.get('/jobSkill', jobSkillController.findAll);
router.post('/jobSkill', jobSkillController.create);
router.get('/jobSkill/:id', jobSkillController.findById);
router.put('/jobSkill/:id', jobSkillController.update);
router.delete('/jobSkill/:id', jobSkillController.delete);

//jobs_status_master
const jobStatusController = require('../controllers/jobStatus.controller');
router.get('/jobStatus', jobStatusController.findAll);
router.post('/jobStatus', jobStatusController.create);
router.get('/jobStatus/:id', jobStatusController.findById);
router.put('/jobStatus/:id', jobStatusController.update);
router.delete('/jobStatus/:id', jobStatusController.delete);

//employer master
const employerController = require('../controllers/employer.controller');
router.get('/employer', employerController.findAll);
router.get('/employer/:page_no', employerController.findAll);
router.post('/employer', employerController.create);
router.post('/employer/:id', employerController.findById);
router.put('/employer/:id', employerController.update);
router.delete('/employer/:id', employerController.delete);

//postJob master
const postJobController = require('../controllers/postJob.controller');
router.get('/postJob/locations', postJobController.getLocations);
router.get('/postJob', postJobController.findAll);
router.get('/postJob/:page_no', postJobController.findAll);
router.post('/postJob', postJobController.create);
router.post('/postJob/:id', postJobController.findById);
router.put('/postJob/:id', postJobController.update); 
router.delete('/postJob/:id', postJobController.delete);

//candidate master
const candidateController = require('../controllers/candidate.controller');
router.get('/candidate', candidateController.findAll);
router.get('/candidate/:page_no', candidateController.findAll);
router.post('/candidate', candidateController.create);
router.post('/candidate/:id', candidateController.findById);
router.put('/candidate/:id', candidateController.update); 
router.delete('/candidate/:id', candidateController.delete);
router.get('/candidate_count',candidateController.getAllCandidateCount);

//educationalQualification master
const educationalQualification = require('../controllers/educationalQualification.controller');
router.get('/educationalQualification', educationalQualification.findAll);
router.get('/educationalQualification/:page_no', educationalQualification.findAll);
router.post('/educationalQualification', educationalQualification.create);
router.post('/educationalQualification/:id', educationalQualification.findById);
router.put('/educationalQualification/:id', educationalQualification.update); 
router.delete('/educationalQualification/:id', educationalQualification.delete);

//jobApplication master
const jobApplication = require('../controllers/jobApplication.controller');
router.get('/jobApplication/:process_status/:job_id', jobApplication.findAllCandidateWithJobStatus);
router.get('/jobApplication_CandidateCountForJob', jobApplication.CandidateCountForJob);


module.exports = router;