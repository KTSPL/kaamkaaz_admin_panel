'use strict';
var dbConn = require('../../config/db.config');
var commonLogger = require("../../common/loggerFn");
const logger = new commonLogger();

//candidate object create
var JobApplication = function (application) {
  this.job_id = candidate.job_id;
  this.candidate_id = candidate.candidate_id;
  this.process_status = candidate.email;
  this.added_date = new Date();
  this.last_updated_date = new Date();
};

JobApplication.findAllCandidateWithJobStatus = (process_status, job_id, result) => {

  dbConn.query(`SELECT
  JA.job_application_id,
  JA.process_status,
  C.candidate_id,
  C.full_name,
  C.email,
  C.mobile,
  C.experience,
  C.resume_path,
  EQ.educational_qualification_id,
  EQ.name,
  ITM.job_category_id AS industry_type_id,
  ITM.name,
  J.job_id,
  J.job_title
  FROM job_applications AS JA
  LEFT JOIN candidate_master AS C ON C.candidate_id = JA.candidate_id
  LEFT JOIN educational_qualification_master AS EQ ON EQ.educational_qualification_id = C.educational_qualification_id
  LEFT JOIN industry_type_master AS ITM ON ITM.job_category_id = C.industry_type_id
  LEFT JOIN job_master AS J ON J.job_id = JA.job_id
  WHERE JA.process_status=? AND J.job_id=?
  `, [process_status, job_id], function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(err, null);
    }
    else {
      result(null, res);
    }
  })
}

JobApplication.CandidateCountForJob = (result) => {

  var myQueries = `SELECT COUNT(job_application_id) AS total_count_for_applied_candidate
  FROM job_applications WHERE process_status=?; 
  SELECT COUNT(job_application_id) AS total_count_for_selected_candidate
  FROM job_applications WHERE process_status=?;
  SELECT COUNT(job_application_id) AS total_count_for_rejected_candidate
  FROM job_applications WHERE process_status=?;`

  dbConn.query(`${myQueries}`,[1,2,3], function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(null, err);
    }
    else {
      logger.info("======================>> job_applications ==> : ", res);
      result(null, res);
    }
  });
};

module.exports = JobApplication;