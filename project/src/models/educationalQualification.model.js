'use strict';
var dbConn = require('../../config/db.config');
var commonLogger = require("../../common/loggerFn");
const logger = new commonLogger();

//EducationalQualification object create
var EducationalQualification = function (education) {
  this.name = education.name;
  this.added_by = 1;
  this.updated_by = 1;
  this.added_date = new Date();
  this.last_updated_date = new Date();
};

EducationalQualification.create = (newEducation, result) => {
  logger.info("======================>> newEducation ==> : ", newEducation);

  dbConn.query("INSERT INTO educational_qualification_master set ?", newEducation, function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(err, null);
    }
    else {
      logger.info("===========>> res.insertId ==> : ", res.insertId);
      result(null, res.insertId);
    }
  });
};

EducationalQualification.findById = (id, result) => {
  dbConn.query("Select educational_qualification_id as id, name from educational_qualification_master where educational_qualification_id = ?  and is_active=1", id, function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(err, null);
    }
    else {
      result(null, res);
    }
  });
};

EducationalQualification.findAll =  (page_no = 0, user_id, role_id, result) => {
  logger.info("======================>> user_id  ==> : ", user_id);
  logger.info("======================>> role_id  ==> : ", role_id);

  let limit  = 10;
  let offset = 0; 
  if (page_no!=0) {
      offset = limit*page_no;
  }
  let cond = '1';
  if(role_id === 2){
      cond = "user_id="+user_id;
  }
  logger.info("======================>> page_no  ==> : ", page_no);

  dbConn.query("Select educational_qualification_id as id, name from educational_qualification_master where "+cond+" limit ?,?", [offset, limit],function (err, res) {
      if(err) {
        logger.error("======================>> error ==> : ", err);
        result(null, err);
      }
      else{
          result(null, res);
      }
  });
};

EducationalQualification.update = (id, education, result) => {
  dbConn.query("UPDATE educational_qualification_master SET name=?, last_updated_date= NOW() WHERE educational_qualification_id = ?", [education.name, id], function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

EducationalQualification.delete = (id, result) => {
  dbConn.query("DELETE FROM educational_qualification_master WHERE educational_qualification_id = ?", [id], function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(null, err);
    }
    else {
      result(null, res);
    }
  });
};

module.exports = EducationalQualification;