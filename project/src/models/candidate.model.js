'use strict';
var dbConn = require('../../config/db.config');
var commonLogger = require("../../common/loggerFn");
const logger = new commonLogger();

//candidate object create
var Candidate = function (candidate) {
  this.educational_qualification_id = candidate.educational_qualification_id;
  this.full_name = candidate.full_name;
  this.email = candidate.email;
  this.mobile = candidate.mobile;
  this.industry_type_id = candidate.industry_type_id;
  this.skill_id = candidate.skill_id;
  this.experience = candidate.experience;
  this.resume_path = candidate.resume_path;
  this.added_by = 1;
  this.updated_by = 1;
  this.added_date = new Date();
};

Candidate.create = (newCandidate, result) => {
  let newCandidateObj = {
    educational_qualification_id: newCandidate.educational_qualification_id,
    full_name: newCandidate.full_name,
    email: newCandidate.email,
    mobile: newCandidate.mobile,
    industry_type_id: newCandidate.industry_type_id,
    experience: newCandidate.experience,
    resume_path: newCandidate.resume_path,
    added_by: 1,
    updated_by: 1,
    added_date: new Date()
  }
  logger.info("====== newCandidateObj =========> : ", newCandidateObj);

  dbConn.query("INSERT INTO candidate_master set ?", newCandidateObj, function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(err, null);
    }
    else {
      let candidateIndustryArr = [];
      for (var i = 0; i < newCandidate.skill_id.length; i++) {
        candidateIndustryArr.push([
          res.insertId,
          newCandidate.skill_id[i]
        ]);

        dbConn.query("INSERT INTO candidate_skills (candidate_id, skill_id) VALUES ?", [candidateIndustryArr], function (err, res) {
          if (err) {
            logger.error("======= candidate skills ======>> error ==> : ", err);
            // res.send('Error');
          }
          else {
            logger.info("======== candidate skills =====>> res.insertId ==> : ", res.insertId);
            //res.send('Success');
          }

        });
        candidateIndustryArr = [];
      }
      logger.info("=============>> res.insertId ==> : ", res.insertId);
      result(null, res.insertId);
    }
  });
};

Candidate.findById = (id, result) => {
  dbConn.query("Select candidate_id as id, educational_qualification_id, full_name, email, mobile, industry_type_id, experience, resume_path from candidate_master where candidate_id = ?", id, function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(err, null);
    }
    else {
      result(null, res);
    }
  });
};

Candidate.findAll = (page_no = 0, user_id, role_id, result) => {
  logger.info("====== user_id =========> : ", user_id);
  logger.info("====== role_id =========> : ", role_id);

  let limit = 10;
  let offset = 0;
  if (page_no != 0) {
    offset = limit * page_no;
  }
  let cond = '1';
  if (role_id === 2) {
    cond = "user_id=" + user_id;
  }
  logger.info("====== page_no =========> : ", page_no);

  dbConn.query("Select candidate_id as id, educational_qualification_id, full_name, email, mobile, industry_type_id, experience, resume_path from candidate_master where " + cond + " limit ?,?", [offset, limit], function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(null, err);
    }
    else {
      result(null, res);
    }
  });
};

Candidate.update = (id, candidate, result) => {
  dbConn.query("UPDATE candidate_master SET educational_qualification_id=?, full_name=?, email=?, mobile=?, industry_type_id=?, experience=?, resume_path=? WHERE candidate_id = ?", [candidate.educational_qualification_id, candidate.full_name, candidate.email, candidate.mobile, candidate.industry_type_id, candidate.experience, candidate.resume_path, id], function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(null, err);
    } else {
      dbConn.query(`DELETE FROM candidate_skills WHERE candidate_id=?`, [id], function (err, result) {
        if (err) {
            logger.error("=============>> error in delete: ==> : ", err);
            result(null, err);
        }
        else {

            for (var i = 0; i < candidate.skill_id.length; i++) {
                let myArr = [id, candidate.skill_id[i]];
                logger.info("======================>> candidate.skill_id[i]:  ==> : ", candidate.skill_id[i]);

                dbConn.query("INSERT INTO candidate_skills (candidate_id, skill_id) VALUES (?)", [myArr], function (err, res) {
                    if (err) {
                        logger.error("======= Candidate Skills ======>> error ==> : ", err);
                        // res.send('Error');
                    }
                    else {
                        logger.info("======== Candidate Skills =====>> res.insertId ==> : ", res.insertId);
                        //res.send('Success');
                    }
                });
                myArr = [];
            }
        }
    });
      result(null, res);
    }
  });
};

Candidate.delete = (id, result) => {
  dbConn.query(`DELETE FROM candidate_master WHERE candidate_id = ?`, [id], function (err, res) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(null, err);
    }
    else {
      result(null, res);
    }
  });
};

Candidate.getAllCandidateCount = (user_id, role_id, result) => {
  logger.info("======================>> user_id  ==> : ", user_id);
  logger.info("======================>> role_id  ==> : ", role_id);

  let cond = '1';
  if (role_id === 2) {
    cond = "user_id=" + user_id;
  }
  dbConn.query(`Select COUNT(*) AS candidate_count from candidate_master where ${cond}`, function (err, rows) {
    if (err) {
      logger.error("======================>> error ==> : ", err);
      result(null, err);
    }
    else {
      logger.info("======================>> candidate_master ==> : ", rows[0]);
      result(null, rows[0]);
    }
  });
};

module.exports = Candidate;