'use strict';
var dbConn = require('./../../config/db.config');
var commonLogger = require("../../common/loggerFn");
const logger = new commonLogger();

//Employer object create
var Employer = function (Employer) {
    this.first_name = Employer.first_name;
    this.last_name = Employer.last_name;
    this.company_name = Employer.company_name;
    this.contact_number1 = Employer.contact_number1;
    this.contact_number2 = Employer.contact_number2;
    this.email = Employer.email;
    this.industry_type_id = Employer.industry_type_id;
    this.website = Employer.website;
    this.core_work = Employer.core_work;
    this.account_status = Employer.account_status;
    this.added_by = 1;
    this.updated_by = 1;
    this.added_date = new Date();
    this.last_updated_date = new Date();
};

Employer.create = (newEmp, result) => {
    let tempEmpCopy = Object.assign({}, newEmp);
    tempEmpCopy.industry_type_id = 0;
    tempEmpCopy.added_by = 1;
    tempEmpCopy.updated_by = 1;
    tempEmpCopy.added_date = new Date();
    tempEmpCopy.last_updated_date = new Date();

    logger.info("======================>> tempEmpCopy ==> : ", tempEmpCopy);

    delete tempEmpCopy.locations;

    dbConn.query("INSERT INTO employer_master set ?", tempEmpCopy, function (err, res) {
        if (err) {
            logger.error("======================>> error ==> : ", err);
            result(err, null);
        }
        else {
            let values = [];
            for (var i = 0; i < newEmp.locations.length; i++) {
                values.push([
                    res.insertId,
                    newEmp.locations[i].address_line1,
                    newEmp.locations[i].address_line2,
                    newEmp.locations[i].landmark,
                    newEmp.locations[i].city,
                    newEmp.locations[i].state,
                    newEmp.locations[i].country,
                    newEmp.locations[i].zip,
                    newEmp.locations[i].geopoint_lat,
                    newEmp.locations[i].geopoint_long
                ]);
            }
            //Bulk insert using nested array [ [a,b],[c,d] ] will be flattened to (a,b),(c,d)
            dbConn.query('INSERT INTO employer_locations (employer_id,address_line1, address_line2,landmark, city, state, country, zip, geopoint_lat, geopoint_long) VALUES ?', [values], function (err, res) {
                if (err) {
                    logger.error("====== Employer Location ======>> error ==> : ", err);
                    // res.send('Error');
                }
                else {
                    logger.info("====== Employer Location =======>> res.insertId ==> : ", res.insertId);
                    //res.send('Success');
                }
            });

            let empIndustryArr = [];
            for (var i = 0; i < newEmp.industry_type_id.length; i++) {
                empIndustryArr.push([
                    res.insertId,
                    newEmp.industry_type_id[i]
                ]);
            }
            dbConn.query("INSERT INTO employer_industry (employer_id, industry_type_id) VALUES ?", [empIndustryArr], function (err, res) {
                if (err) {
                    logger.error("======= Employer Industry ======>> error ==> : ", err);
                    // res.send('Error');
                }
                else {
                    logger.info("======== Employer Industry =====>> res.insertId ==> : ", res.insertId);
                    //res.send('Success');
                }
                delete tempEmpCopy.industry_type_id;
            });

            logger.info("======================>> res.insertId ==> : ", res.insertId);
            result(null, res.insertId);
        }
    });
};

Employer.createUser = (newUser, result) => {
    dbConn.query("INSERT INTO user_master set ?", newUser, function (err, res) {
        if (err) {
            logger.error("======================>> error ==> : ", err);
            result(err, null);
        }
        else {
            logger.info("======================>> res.insertId ==> : ", res.insertId);
            result(null, res.insertId);
        }
    });
};

Employer.findById = (id, result) => {
    dbConn.query(`Select 
    E.employer_id as id,
    E.first_name,
    E.last_name,
    E.company_name,
    E.contact_number1,
    E.contact_number2,
    E.email,
    GROUP_CONCAT(EI.industry_type_id) AS industry_type_id
    from employer_master AS E
    LEFT JOIN employer_industry AS EI ON EI.employer_id = E.employer_id
    where E.employer_id = ? and E.is_active=1`, id, function (err, res) {
        if (err) {
            logger.error("======================>> error ==> : ", err);
            result(err, null);
        }
        else {
            let finalRS = {};
            finalRS = res;
            if (res.length > 0) {
                res[0]["industry_type_id"] = res[0]["industry_type_id"].split(",");

                dbConn.query("Select * from employer_locations where employer_id = ? and is_active=1", id, function (err, locationRes) {
                    if (err) {
                        logger.error("======================>> error ==> : ", err);
                        result(err, null);
                    }
                    else {
                        finalRS[0]["locations"] = locationRes;
                        logger.info("======================>> finalRS:  ==> : ", finalRS);
                        result(null, finalRS);
                    }
                });
            } else {
                result(null, res);
            }
        }
    });
};

Employer.findAll = (page_no = 0, userObj, result) => {

    let limit = 10;
    let offset = 0;
    if (page_no != 0) {
        offset = limit * page_no;
    }
    logger.info("======================>> page_no, offset:  ==> : ", page_no, offset);

    /* show list only for super admin ( role_id =1) */
    if (userObj.role_id == 1) {
        dbConn.query("Select employer_id as id,first_name,last_name,company_name,contact_number1,contact_number2,email,industry_type_id from employer_master where 1 limit ?,?", [offset, limit], function (err, res) {
            if (err) {
                logger.error("======================>> error ==> : ", err);
                result(null, err);
            }
            else {
                result(null, res);
            }
        });
    } else {
        result(null, null);
    }
};

Employer.update = (id, employerObj, result) => {
    dbConn.query(`UPDATE employer_master SET 
    first_name=?, last_name=?,company_name=?, contact_number1=?, contact_number2=?, email=?,
    website=?, core_work=?, account_status=?, last_updated_date=? WHERE employer_id = ?`,
        [employerObj.first_name, employerObj.last_name, employerObj.company_name, employerObj.contact_number1, employerObj.contact_number2, employerObj.email, employerObj.website, employerObj.core_work, employerObj.account_status, employerObj.last_updated_date, id],
        function (err, res) {
            if (err) {
                logger.error("======================>> error for update ==> : ", err);
                result(null, err);
            } else {
                let values = [];
                logger.info("======================>>  employerObj.locations:  ==> : ", employerObj.locations);

                for (var i = 0; i < employerObj.locations.length; i++) {
                    values = [
                        employerObj.locations[i].address_line1,
                        employerObj.locations[i].address_line2,
                        employerObj.locations[i].landmark,
                        employerObj.locations[i].city,
                        employerObj.locations[i].state,
                        employerObj.locations[i].country,
                        employerObj.locations[i].zip,
                        employerObj.locations[i].geopoint_lat,
                        employerObj.locations[i].geopoint_long,
                        id
                    ];
                    logger.info("======================>> values:  ==> : ", values);


                    dbConn.query(`UPDATE employer_locations SET address_line1=?, address_line2=?, landmark=? ,city=?, state=?, country=?, zip=?, geopoint_lat=?, geopoint_long=? WHERE employer_id=?`, values, function (err, result) {
                        if (err) {
                            logger.error("=============>> error update bulk: ==> : ", err);
                            result(null, err);
                        }
                        else {
                            //res.send('Success');
                        }
                    });

                    // values = [];
                }

                dbConn.query(`DELETE FROM employer_industry WHERE employer_id=?`, [id], function (err, result) {
                    if (err) {
                        logger.error("=============>> error in delete: ==> : ", err);
                        result(null, err);
                    }
                    else {

                        for (var i = 0; i < employerObj.industry_type_id.length; i++) {
                            let myArr = [id, employerObj.industry_type_id[i]];
                            logger.info("======================>> employerObj.industry_type_id[i]:  ==> : ", employerObj.industry_type_id[i]);

                            dbConn.query("INSERT INTO employer_industry (employer_id, industry_type_id) VALUES (?)", [myArr], function (err, res) {
                                if (err) {
                                    logger.error("======= Employer Industry ======>> error ==> : ", err);
                                    // res.send('Error');
                                }
                                else {
                                    logger.info("======== Employer Industry =====>> res.insertId ==> : ", res.insertId);
                                    //res.send('Success');
                                }
                            });
                            myArr = [];
                        }
                    }
                });
                //Bulk insert using nested array [ [a,b],[c,d] ] will be flattened to (a,b),(c,d)
                result(null, res);
            }
        });
};

Employer.delete = (id, result) => {
    dbConn.query("DELETE FROM employer_master WHERE employer_id = ?", [id], function (err, res) {
        if (err) {
            logger.error("======================>> error ==> : ", err);
            result(null, err);
        }
        else {
            result(null, res);
        }
    });
};

module.exports = Employer;