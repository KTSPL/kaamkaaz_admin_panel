'use strict';
const Candidate = require('../models/candidate.model');
const Joi = require('joi');
var commonLogger = require("../../common/loggerFn");
const logger = new commonLogger();

/** request data validation  */
const create_schema = Joi.object({
  educational_qualification_id : Joi.number().required(),
  full_name : Joi.string().min(1).required(),
  email : Joi.string().email().min(1).required(),
  mobile : Joi.string().min(10).max(15).required(),
  industry_type_id :Joi.number().min(1).required(),
  skill_id : Joi.array(),
  experience : Joi.number().required(),
  resume_path : Joi.string().min(1).required() 
});

const findbyid_schema = Joi.object({
  id : Joi.number()
});
/** End request data validation*/

exports.findAll = (req, res) => {
  
  Candidate.findAll(req.params.page_no, req.user.user_id,req.user.role_id, (err, objResultSet) => {
    logger.info("==========>> Controller <<=======");
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
       });
    }
    res.send({
      success:true,
      error_code:"",
      message:"",
      data:objResultSet
    });
  });
};

exports.create = (req, res) => {
  const new_candidate = new Candidate(req.body);
  const { error } = create_schema.validate(req.body);
  if(error){
    res.status(400).send({ success:false, message: 'Wrong parameter value', error });
  }
  
  Candidate.create(new_candidate, (err, objResultSet) => {
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
        });
    }
    res.json(
      {
        success:true,
        error_code:"",
        message:"Candidate Added Successfully!",
        data:objResultSet
      });
  });
};

exports.findById = function(req, res) {
  Candidate.findById(req.params.id, function(err, objResultSet) {
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
       });
    }
    res.json({
      success:true,
      error_code:"",
      message:"",
      data:objResultSet
    });
  });
};

exports.update = function(req, res) {
  const { error } = create_schema.validate(req.body);
  if(error){
    res.status(400).send({ success:false, message: 'Wrong parameter value', error });
  }

  Candidate.update(req.params.id, new Candidate(req.body), function(err, objResultSet) {
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
        });
    }
    res.json(
      { 
        success:true, 
        message: 'Candidate Details Successfully Updated',
        error_code:"",
        data: []
      });
  });
};

exports.delete = function(req, res) {
  Candidate.delete( req.params.id, function(err, objResultSet) {
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
       });
    }
    res.json(
      {
        success:true,
        message: 'Candidate successfully deleted',
        error_code:"",
        data: []
      });
  });
};

exports.getAllCandidateCount = (req, res) => {
  Candidate.getAllCandidateCount(req.user.user_id, req.user.role_id, (err, objResultSet) => {
    logger.info("========>> Controller <<========");
    if (err) {
       res.send({
        success: false,
        message: 'Something went wrong!',
        error_code: '',
        data: err
      });
    }
    logger.info("======================>> res  ==> : ", objResultSet);

    res.send({
      success: true,
      message: "",
      data: objResultSet
    });
  });
};