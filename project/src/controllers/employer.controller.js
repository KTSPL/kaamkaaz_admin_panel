'use strict';
const objEmployer = require('../models/employer.model');
const objMailer = require("../../common/mailer");
const Joi = require('joi');
const bcrypt = require('bcrypt-nodejs');
const saltRounds = 10; 
var commonLogger = require("../../common/loggerFn");
const logger = new commonLogger();

/** request data validation  */
const create_schema = Joi.object({
  first_name     : Joi.string().min(1).required(),
  last_name      : Joi.string().min(1).required(),
  company_name   : Joi.string().min(1).required(),
  contact_number1: Joi.string().optional(),
  contact_number2: Joi.string().optional(),
  email          : Joi.string().email().required(),
  industry_type_id : Joi.array(),
  website : Joi.string().required(),
  core_work : Joi.string().min(1).required(),
  account_status : Joi.number().min(1).max(2).required(),
  locations      : Joi.array().items(
    Joi.object({
      address_line1: Joi.string().required(),
      address_line2: Joi.string().required(),
      landmark : Joi.string().optional(),
      city: Joi.string().required(),
      state: Joi.string().required(),
      country: Joi.string().required(),
      zip: Joi.string().required(),
      geopoint_lat: Joi.string().required(),
      geopoint_long: Joi.string().required()
  })).required()
});

const findbyid_schema = Joi.object({
  id : Joi.number()
});
/** End request data validation*/

// get all records
exports.findAll = (req, res) => {
  console.log("By Rishabh Line 3",req.body)
  logger.info("======================>> req obj in jobtype ==> : ", req.user);

  objEmployer.findAll(req.params.page_no, req.user,(err, objResultSet) => {
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
       });
    }
    res.send({
      success:true,
      error_code:"",
      message:"call successful",
      data:objResultSet
    });
  });
};

exports.create = (req, res) => {
  const new_emp = new objEmployer(req.body);
  //handles null error
  const { error } = create_schema.validate(req.body);
  if(error){
     return res.status(401).send({ success:false, message: 'Wrong parameter value', error });
  }
  
  objEmployer.create(req.body, (err, objResultSet) => {
    if (err) {
      return res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
        });
    }
    const hash = bcrypt.hashSync(new_emp.first_name);
    
    let userObj = {
      "user_name" : new_emp.first_name,
      "password" : hash,
      "role_id" : 2,
      "employer_id" : objResultSet
    }
    objEmployer.createUser(userObj, (err, objUserResultSet) => {
      let htmlBody = '<p><b>Hello ' + new_emp.first_name+' '+
      '<p>Here are the login details of the KamKaj dashboard:<br/></p>' +
      '<p>URL : http://ktspldemo.co.in/kk_test/<br/></p>' +
      '<p>Username : '+ new_emp.first_name +'<br/></p>' +
      '<p>Password : '+ new_emp.first_name +'<br/></p>';
      
      objMailer.sendEmail(new_emp.email,"Kamkaj - Dashboard login details",htmlBody, null, null);
      res.send(
        {
          success:true,
          error_code:"",
          message:"Employer added successfully!",
          data:objResultSet
        });
    });
  });
};

exports.findById = (req, res)=> {
  console.log("By Rishabh Line 2",req.body)
  const { error } = findbyid_schema.validate(req.params);
  if(error) {
    return res.send({ success:false, message: 'Wrong parameter value' , error});
  }
  objEmployer.findById(req.params.id, function(err, objResultSet) {
    if (err) {
      return res.send({
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
       });
    }
    res.send({
      success:true,
      error_code:"",
      message:"",
      data:objResultSet
    });
  });
};

exports.update = function(req, res) {

  const { error } = create_schema.validate(req.body);
  if(error){
    return res.status(400).send({ success:false, message: 'Wrong parameter value', error });
  }
  objEmployer.update(req.params.id, req.body, function(err, emp) {
    if (err) {
      return res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
        });
    }
    res.send(
      { 
        success:true, 
        message: 'Employer successfully updated',
        error_code:"",
        data: []
      });
  });
};

exports.delete = function(req, res) {
  objEmployer.delete( req.params.id, function(err, emp) {
    if (err) {
      return res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
       });
    }
    res.send(
      {
        success:true,
        message: 'Employer successfully deleted',
        error_code:"",
        data: []
      });
  });
};