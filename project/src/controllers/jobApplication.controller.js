'use strict';
const JobApplication = require('../models/jobApplication.model');
const Joi = require('joi');
var commonLogger = require("../../common/loggerFn");
const logger = new commonLogger();

/** request data validation  */

const findbystatus_schema = Joi.object({
  process_status: Joi.number().min(1).max(3).required(),
  job_id: Joi.number().required()
});

/** End request data validation*/

exports.findAllCandidateWithJobStatus = function (req, res) {
  const { error } = findbystatus_schema.validate(req.params);
  if (error) {
    return res.send({ success: false, message: 'Wrong parameter value', error });
  }
  JobApplication.findAllCandidateWithJobStatus(req.params.process_status, req.params.job_id, function (err, objResultSet) {
    logger.info("==========>> Controller <<=======");

    if (err) {
      return res.send({
        success: false,
        message: 'Something went wrong!',
        error_code: '',
        data: err
      });
    }
    res.send({
      success: true,
      error_code: "",
      message: "",
      data: objResultSet
    });
  });
};

exports.CandidateCountForJob = (req, res) => {

  JobApplication.CandidateCountForJob((err, objResultSet) => {
    logger.info("========>> Controller <<========");
    if (err) {
      res.send({
        success: false,
        message: 'Something went wrong!',
        error_code: '',
        data: err
      });
    }
    var objResultSetArray = [];
    objResultSet.map(res => {
      objResultSetArray.push(res[0])
    });
  
    res.send({
      success: true,
      message: "",
      data: objResultSetArray
    });
  });
};