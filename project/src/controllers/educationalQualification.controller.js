'use strict';
const EducationalQualification = require('../models/educationalQualification.model');
const Joi = require('joi');
var commonLogger = require("../../common/loggerFn");
const logger = new commonLogger();

/** request data validation  */
const create_schema = Joi.object({
  name : Joi.string().min(1).required() 
});

const findbyid_schema = Joi.object({
  id : Joi.number()
});
/** End request data validation*/

// get all records
exports.findAll = (req, res) => {
  
  EducationalQualification.findAll(req.params.page_no, req.user.user_id,req.user.role_id, (err, objResultSet) => {
    logger.info("==========>> Controller <<=======");
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
       });
    }
    res.send({
      success:true,
      error_code:"",
      message:"",
      data:objResultSet
    });
  });
};

exports.create = (req, res) => {
  const new_educational_qualification = new EducationalQualification(req.body);
  const { error } = create_schema.validate(req.body);
  if(error){
    res.status(400).send({ success:false, message: 'Wrong parameter value', error });
  }
  
  EducationalQualification.create(new_educational_qualification, (err, objResultSet) => {
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
        });
    }
    res.json(
      {
        success:true,
        error_code:"",
        message:"Educational Qualification Added Successfully!",
        data:objResultSet
      });
  });
};

exports.findById = function(req, res) {
  const { error } = findbyid_schema.validate(req.params);
  if(error) {
    res.status(400).send({ success:false, message: 'Wrong parameter value' , error});
  }
  EducationalQualification.findById(req.params.id, function(err, objResultSet) {
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
       });
    }
    res.json({
      success:true,
      error_code:"",
      message:"",
      data:objResultSet
    });
  });
};

exports.update = function(req, res) {
  const { error } = create_schema.validate(req.body);
  if(error){
    res.status(400).send({ success:false, message: 'Wrong parameter value', error });
  }

  EducationalQualification.update(req.params.id, new EducationalQualification(req.body), function(err, objResultSet) {
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
        });
    }
    res.json(
      { 
        success:true, 
        message: 'Educational Qualification Details Successfully Updated',
        error_code:"",
        data: []
      });
  });
};

exports.delete = function(req, res) {
  EducationalQualification.delete( req.params.id, function(err, objResultSet) {
    if (err) {
      res.send({ 
        success:false, 
        message: 'Something went wrong!',
        error_code: '',
        data:err
       });
    }
    res.json(
      {
        success:true,
        message: 'Educational Qualification successfully deleted',
        error_code:"",
        data: []
      });
  });
};